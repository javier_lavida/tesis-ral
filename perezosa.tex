\section{Evaluación perezoza}

\textsc{Haskell}, entre otras cosas, se caracteriza por el concepto de que su evaluación es perezoza y este concepto puede ser explotado para mejorar el tiempo de ejecución ciertas funciones. Empecemos recordando el concepto de amortización. 

El concepto de amortización surge al considerar $n$ operaciones cuyo tiempo de ejecución queremos restringir, sin importarnos el tiempo de ejecución de cada una de las operaciones. Por ejemplo, nos gustaría que toda la secuencia de operaciones fuese ejecutada en $O(n)$ sin forzar a que cada operación sea ejecutada en $O(1)$. En dicho caso, diremos que las $n$ operaciones se ejecutan en $O(n)$ amortizado.

Para demostrar que se cumple una cota amortizada, se define el costo de cada operación y se demuestra que para cualquier secuencia de operaciones se cumple que el costo total amortizado de las operaciones es mayor o igual que el costo real, es decir:

\[
\sum\limits_{i=1}^m a_i \geq \sum\limits_{i=1}^m t_i
\]

Con $a_i$ el costo amortizado de la operación $i$, $t_i$ el costo real de la operación $i$ y $m$ el número total de operaciones. Aunque de hecho, usualmente se demuestra un hecho más fuerte: En cualquier subsecuencia operaciones, el costo acumulado amortizado es una cota superior del costo real acumulado, es decir:

\[
\sum\limits_{i=1}^j a_i \geq \sum\limits_{i=1}^j t_i,  \forall j \leq m
\]

La diferencia entre el costo amortizado y el costo real acumulado se conoce como \emph{ahorros acumulados}. Es fácil ver que los ahorros siempre son mayores o iguales a 0.

Utilizar el concepto de amortización nos permite tener operaciones cuyo costo real sea mayor al costo amortizado, estas operaciones las denominaremos operaciones \emph{costosas} y existirán también operaciones cuyo costo real sea menor al costo amortizado, a estas las llamaremos operaciones \emph{baratas}. Las operaciones costosas reducen nuestros ahorros, mientras que las operaciones baratas los aumentan. La clave para demostrar que nuestras operaciones tienen una cota amortizada es demostrar que las operaciones costosas solo ocurren cuando nuestros ahorros son suficientes como para cubrirlas.

Existen dos métodos principales para demostrar una cota amortizada: \emph{el método del banquero} y \emph{el método del físico}. Para nuestro objetivo, nos concentraremos en el primero.

En el método del baquero, el ahorro acumulado es representado como \emph{créditos} que están asociados con ubicaciones específicas en la estructura de datos. Estos créditos son utilizados para {\it pagar} acceso a esas ubicaciones posteriormente. El costo amortizado de cada operación es definido como el costo real de la operación más los créditos obtenidos en la operación menos los créditos empleados por la operación:
\[
a_i = t_i + c_i - \bar{c_i}
\] 
donde $c_i$ son los créditos obtenidos por la operación $i$ y $\bar{c_i}$ son los créditos que utiliza la operación $i$. Cada crédito debe de tenerse antes de poder gastarlo y ningún crédito puede utilizarse más de una vez. Por lo tanto se cumple que: $\sum c_i \geq \sum \bar{c_i}$ que garantiza que $\sum a_i \geq \sum t_i$. Las demostraciones que utilizan este método definen un \emph{invariante crediticio} que distribuye los créditos de tal manera que, cada que ocurra una operación costosa, se han otorgado suficientes créditos para cubrir su costo.

Para ilustrar como funciona el método del banquero, mostraremos un breve ejemplo con colas. Las colas son una estructura de datos con las operaciones \texttt{queue} y \texttt{dequeue}, que significan encolar y decolar. Encolar significa añadir un elemento a la cola y decolar, quitar un elemento. Todo esto en orden \emph{FIFO (First in first out)}, es decir, tras múltiples operaciones de encolar, al hacer una operación de decolar, el elemento que debe salir debe de ser el primero que fue encolado.

La implementación de colas más común en un sentido puramente funcional, consiste en un par de listas $f$ y $r$ representando $f$ los primeros elementos de la cola en orden natural y $r$ los elementos del final de la cola en orden inverso. Por ejemplo, la cola con los elementos del 1 al 6 es representada por las listas \texttt{f=[1,2,3]} y \texttt{r=[6,5,4]}

La implementación en Haskell:

\begin{lstlisting}

data Queue a = ([a], [a])

\end{lstlisting}

Con esta representación, la cabeza de la cola, es el primer elemento de la primer lista. Este elemento es tomado o descartado por las operaciones \texttt{head} y \texttt{tail} respectivamente. 

\begin{lstlisting}

head :: Queue a -> a
head (x : f, r) = x

tail :: Queue a -> Queue a
tail (x : f, r) = (f, r)
\end{lstlisting}

De manera similar, el último elemento de la cola es el primer elemento de la lista $r$, así que la función \texttt{snoc} se define fácilmente:

\begin{lstlisting}

snoc :: Queue a -> a -> Queue a
snoc (f, r) x = (f, x : r)

\end{lstlisting}

Con las funciones definidas podemos ver que los elementos son removidos de $f$ pero añadidos a $r$, por lo que debería existir alguna manera para migrar elementos de $r$ a $f$. Esta migración puede lograrse obteniendo la reversa de $r$ y asignándola como nueva $f$, dejando a $r$ como la lista vacía. El objetivo es mantener el invariante que $f$ es vacía solo si $r$ también está vacía, es decir, si toda la cola está vacía. Si este invariante no se mantuviera y $f$ fuera vacía mientras que $r$ no, el primer elemento de la cola sería el último elemento de la lista $r$, al cual tendríamos acceso en $O(n)$. Manteniendo este invariante podemos garantizar que el acceso al primer elemento se realiza en orden constante.

Modificaremos las funciones \texttt{snoc} y \texttt{tail} para detectar los casos que romperían el invariante y lo corregiremos.

\begin{lstlisting}

snoc :: Queue a -> a -> Queue a
snoc ([], _) x = ([x], [])
snoc (f, r) x = (f, x : r)

tail :: Queue a -> Queue a
tail ([x], r) = (reverse r, [])
tail (x : f, r) = (f, r)

\end{lstlisting}

Podemos observar que solo añadimos el caso cuando \texttt{snoc} recibe una cola con $f$ como la lista vacía. Ignoramos por completo la lista $r$ porque sabemos que nuestro invariante dice que esta lista está vacía. Aunque si la entrada por si misma rompía el invariante, en efecto, nuestras funciones devolverían una cola cumpliendo el invariante pero eliminarían los elementos de la lista $r$. Una solución más elegante es crear una función \texttt{checkf} que corregirá colas que no cumplan el invariante. Así nuestra definición final de \texttt{snoc} y \texttt{tail} será:

\begin{lstlisting}

checkf :: Queue a -> Queue a
checkf ([], r) = (reverse r, [])
checkf q = q

snoc (f, r) x = checkf (f, x : r)

tail :: Queue a -> Queue a
tail (x : f, r) = checkf (f, r)

\end{lstlisting}

La operación \texttt{snoc} toma $O(1)$ y la operación \texttt{tail} toma $O(n)$ en el peor caso. Sin embargo, podemos demostrar que ambas operaciones toman $O(1)$ amortizado utilizando el método del banquero.

Mantenemos el {\it invariante crediticio} que cada elemento en la lista $r$ está asociada a un crédito. El uso de la operación \texttt{snoc} toma un único paso y guarda un crédito asociado al nuevo elemento de la lista $r$, resultando esto en un costo amortizado total de 2 ya que $a_i=t_i+c_i-\bar{c_i}=1+1-0$. Cada llamada a la función \texttt{tail} que no requiere obtener la reversa de la lista $r$, requiere un solo paso y no gasta ni guarda créditos, para un costo amortizado de 1. Finalmente, las llamadas a \texttt{tail} que calculan la reversa de $r$ toman $m+1$ pasos, con $m$ la longitud de la lista $r$ y gasta los créditos guardados en $r$, que eran en total $m$, resultando en un costo amortizado de $a_i= t_i + c_i - \bar{c_i}= (m+1) + 0 - m=1$.




