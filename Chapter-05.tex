\backmatter
\chapter{Conclusiones}

En este trabajo hemos presentado a la estructura de datos conocida como lista de acceso aleatorio, la cual, ofrece al programador funcional operaciones eficientes de búsqueda y actualización de elementos. Nuestras implementaciones son ejemplos de representaciones numéricas basadas en sistemas numéricos binarios. Es decir, una estructura de tamaño $n$ se obtiene a partir de la representación binaria del número $n$. En total desarrollamos 5 implementaciones que incluyen funciones de acceso eficientes (de $O(\log n)$) resumidas a continuación:

\begin{itemize}
\item
\emph{LAA basadas en el sistema binario estándar.} Esta implementación causa ineficiencias en las operaciones de listas comunes.
\item
\emph{LAA basadas en sistema binario sin cero.} Esta implementación mejora la versión anterior, recuperando la eficiencia de la función \texttt{head}.
\item
\emph{LAA basadas en el sistema binario sesgado.} Se recupera la eficiencia de las funciones de listas comunes. Esta implementación resulta ser la más eficiente de todas. 
\item
\emph{LAA con tipos anidados de primer orden, basadas en el sistema binario estándar.} Esta implementación ofrece una funcionalidad igual a nuestra primera implementación pero capturando estáticamente la noción de LAA válida.
\item
\emph{LAA con tipos anidados de orden superior, basadas en el sistema binario sin cero.} Esta implementación ofrece una funcionalidad igual a nuestra segunda implementación pero capturando estáticamente la noción de LAA válida mediante el uso de constructores de tipo.
\end{itemize}

Debido a que en todos los casos, las operaciones de acceso resultan eficientes, cualquiera de estas implementaciones puede servir como una implementación funcional de arreglos. Más aún, en nuestro caso no se requiere a priori una cota para el tamaño inicial del arreglo, puesto que este puede crecer arbitrariamente. Es importante observar que a pesar de que nuestras LAA resultan ser eficientes para implementar arreglos, sus tiempos de ejecución son inferiores a los correspondientes para el caso de arreglos imperativos, aquellos son $O(\log n)$ y éstos son $O(1)$. Para una discusión más profunda sobre el concepto de arreglo funcional y sus implementaciones veáse \cite{grue}


Hemos dicho que las operaciones definidas sobre las listas de acceso aleatorio, a excepción de las funciones de acceso, son análogas a funciones sobre el sistema numérico, base de la definición de dichas listas. Sin embargo, surge la duda ¿qué ocurre con la función suma? Esta operación, en las listas comunes, corresponde a la función de concatenación y se define de la siguiente manera:

\begin{lstlisting}
append :: List a -> List a -> List a
append Nil l = l
append (Cons x xs) l = Cons x (append xs l) 
\end{lstlisting}

Que claramente es análoga a la función suma en el sistema unario, definida de la siguiente manera:

\begin{lstlisting}
add :: Nat -> Nat -> Nat
add Zero m = m
add (Succ n) m = Succ (add n m)
\end{lstlisting}

En este sistema numérico, la función \texttt{append} es efectivamente análoga a la función \texttt{add}. Sin embargo, cuando cambiamos de sistema numérico, el algoritmo usual para sumar consiste en sumar dígito a dígito, llevando un acarreo tal como lo hacemos en el sistema decimal usual.

Siguiendo esta idea, podríamos implementar la función \texttt{append} en las listas de acceso aleatorio, pero al sumar dos dígitos no nulos, nos veríamos obligados a combinar los árboles que éstos tienen asociados. Resultando esto, en una mezcla de los elementos, situación indeseable puesto que el resultado dela función \texttt{append} debe ser una lista que contiene en primera instancia a los elementos de la primera LAA seguidos de los de la segunda.

Esta idea funciona en otras representaciones numéricas, como \emph{montículos binomiales}, donde el orden en el que se guarden los elementos no importa, a excepción de preservar que el elemento mínimo o máximo del árbol esté en la raíz.

Como una posible línea de trabajo futuro, mencionamos la implementación de listas de acceso aleatorio con tipos anidados basadas en el sistema binario sesgado. Recordemos que para esta implementación, necesitamos árboles con información en todos los nodos, los cuales pueden definirse de la siguiente manera:

\begin{lstlisting}

data Perfect bush a = Zero (bush a)
                    | Succ (Perfect (Fork bush) a)

data Fork bush a = Fork (bush a) a (bush a)
\end{lstlisting}

Obsérvese que lo único que se hizo con respecto a la definición anterior de \texttt{Perfect} y \texttt{Fork} fue modificar el constructor de árboles \texttt{Fork} para que incluya información en el nodo raíz, con lo cual se consiguen árboles con información en todos los nodos. El principal obstáculo para esta implementación radica en el hecho de que el sistema empleado es disperso, por lo que no se pueden hacer explícitos los dígitos nulos y esta característica resulta muy difícil de capturar con la clase de tipos anidados que hemos utilizado hasta ahora. 

