\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[spanish]{babel}

\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{amsmath}

\usepackage{listings}
\usepackage{mathtools}

\usepackage{tikz}
\usepackage{default}
\usepackage{xcolor}
\usepackage{alltt}

\lstset{basicstyle=\ttfamily\footnotesize,breaklines=true}

\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\DeclarePairedDelimiter\pt{\langle}{\rangle}

\definecolor{DarkBlue}{HTML}{00165E}
\usetheme{CambridgeUS}
\useinnertheme{rectangles}
\useoutertheme{infolines}

\setbeamercolor{frametitle}{fg=DarkBlue,bg=DarkBlue!20}
\setbeamercolor{section in head/foot}{bg=DarkBlue}
\setbeamercolor{author in head/foot}{bg=DarkBlue}
\setbeamercolor{date in head/foot}{fg=DarkBlue}

\usebackgroundtemplate{
  \tikz[overlay,remember picture] 
  \node[opacity=0.1, at=(current page.south east),anchor=south east,inner sep=0pt] {
    \includegraphics[height=\paperheight,width=\paperwidth]{BG.png}};
}


\title{Listas de acceso aleatorio}
\author[F. Javier Enríquez Lávida]{Francisco Javier Enríquez Lávida}
\institute[UNAM] % (optional)
{
  Facultad de Ciencias\\
  UNAM
}
\date[Dic 2015] % (optional)
{Diciembre 2015}
\subject{Ciencias de la Computación}
\begin{document}

\frame{\titlepage}

\section{Preliminares}

\frame{
    \frametitle{Listas vs. Arreglos}
    \begin{itemize}
        \item<1-> Recordemos que las listas comunes tienen la propiedad de permitir añadir y quitar elementos a la cabeza de manera eficiente.
        \item<2-> No obstante, buscar y actualizar elementos no es tan eficiente. Hay que recorrer los elementos anteriores al elemento buscado.
        \item<3 -> Recordemos también que los arreglos resuelven este problema. Buscar y actualizar elementos se hace de manera eficiente.
        \item<4 -> Sin embargo, el tamaño de un arreglo es constante. Es decir, añadir y quitar elementos no es eficiente

    \end{itemize}
}

\frame{
    \frametitle{¿Qué queremos lograr?}

    Queremos una estructura que tenga las siguientes operaciones:
    \begin{itemize}
        \item<1 ->
        Añadir un elemento a la cabeza de una lista
        \begin{alltt}
        cons :: a \(\rightarrow \) RAL a \(\rightarrow \) RAL a\\
        \end{alltt}
        \item<2 ->
        Separar una lista en cabeza y cola.
        \begin{alltt}
        uncons :: RAL a \(\rightarrow \) (a, RAL a)\\
        \end{alltt}
        \item<3 ->
        Obtener el elemento en la posición $i$
        \begin{alltt}
        lookup :: RAL a \(\rightarrow \) Int \(\rightarrow \)  a\\
        \end{alltt}
        \item<4 ->
        Modificar el elemento en la posición $i$
        \begin{alltt}
        update :: RAL a \(\rightarrow \) Int \(\rightarrow \) a \(\rightarrow \) RAL a\\
        \end{alltt}
        \item<5 ->
        Además, queremos que realice estas operaciones de manera eficiente.
    \end{itemize}
}

\frame{
    \frametitle{¿Cómo lo logramos?}
    \begin{itemize}
        \item<1 ->
        Para buscar elementos dentro de una lista común, hay que recorrer todos los elementos anteriores al elemento buscado uno por uno. ¿Podemos evitar recorrerlos uno por uno?
        \item<2 ->
        Utilicemos una representación numérica binaria. A cada dígito le asociamos un árbol binario perfecto.
        \item<3 ->
        ¿Qué es un árbol binario perfecto? Es un árbol binario donde cada nodo tiene exactamente dos hijos y todas las hojas están al mismo nivel.
        \item<4 ->
        El rango de un árbol binario perfecto es la altura del árbol. Se cumple la propiedad que el árbol tiene $2^i$ elementos. Con $i$ el rango del árbol.
    \end{itemize}
}

\section{Representaciones numéricas}

\begin{frame}
    \frametitle{Sistemas numéricos}
    Un \emph{sistema numérico} es la manera de representar un número natural.
    \begin{itemize}
        \item
        El sistema numérico más simple son los numerales de Peano
        \begin{itemize}
            \item
            El cero es un número natural
            \item
            El sucesor de un número natural, es un número natural
            \item
            \texttt{Nat := Zero | Succ (Nat)}
        \end{itemize}
    \end{itemize}
    Éste es un \emph{sistema numérico no posicional}. 
\end{frame}

\begin{frame}
    \frametitle{Sistemas numéricos posicionales}
    \begin{itemize}
    \item<1 ->
    Un sistema numérico posicional está dado por una tupla $<D,W>$.
    \item<2 ->
    $D$ un conjunto de dígitos, usualmente $D \subset \mathbb{N}$.
    \item<3 ->
    $W$ un conjunto de pesos.
    \item<4 ->
    La representación del $n \in \mathbb{N}$ está dada por $d_0d_1d_2\dots d_k$ tales que:
    \[
        d_0\cdot w_0 + d_1\cdot w_1 + \dots + d_k\cdot w_k = n
    \]
    \item<5 ->
    Así $n\approx d_0d_1d_2\dots d_k$ 
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Sistemas numéricos}{}
    \begin{itemize}
        \item<1 -> Sistema decimal usual: $D=\{0,1,\ldots, 0\},\;W=\{10^i\;|\;i\in\mathbb{N}\}$
        \[
        5 \approx 5\;\;\;1234\approx 4321 
        \]
        \item<2 -> Sistema binario común: $D=\{0,1\},\;W=\{2^i\;|\;i\in\mathbb{N}\}$
        \[
        5\approx 101 %= 1\cdot 2^0 + 0\cdot 2^1 + 2\cdot 2^2
        \]
        \item<3 -> Sistema binario con $1,2$ (sin cero): $D=\{1,2\},\;W=\{2^i\;|\;i\in\mathbb{N}\}$
        \[
        5 \approx 12 %= 1\cdot 2^0 + 2\cdot 2^1 
        \]
        \item<4 -> Sistema binario con $0,1,2$ (múltiples representaciones) : $D=\{0,1,2\},\;W=\{2^i\;|\;i\in\mathbb{N}\}$
        \[
        5 \approx 12\;\;\; 5 \approx 101 
        \]
        % \item Sistema unario (sin cero): $D=W=\{1\}$
        % \beqs
        % 5=11111= 1\cdot 1^0 + 1\cdot 1^1 + 1\cdot 1^2 + 1\cdot 1^3 + 1\cdot 1^4
        % \]



        % \item Sistema binario de Fibonacci: $D=\{0,1\},\;W=\{F_i\;|\;i\in\mathbb{N}\}$ donde
        % $F_i$ es el i-ésimo número de Fibonacci ($1,1,2,3,5,8,13,\ldots$):
        % \[
        % 5 = 00001=0011\;\;\;\;7=00101
        % \]
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Representaciones numéricas}
    Recapitulando, la representación de un número natural con un sistema numérico no posicional puede ser definida como:
    \begin{align*}
        \texttt{data Nat} &= \texttt{Zero}\\
        &|\ \texttt{Succ (Nat)}
    \end{align*}
    Y recordando también la definición de una lista:
    \begin{align*}
        \texttt{data List a} &= \texttt{Nil}\\
        &|\ \texttt{Cons a (List a)}
    \end{align*}

    Podemos apreciar una notable analogía. 
\end{frame}

\begin{frame}
    \frametitle{Representaciones numéricas}
        \begin{itemize}
        \item Sirviendonos de esta analogía podemos implementar estructuras de datos mediante las representaciones numéricas
        \item La estructura de datos hereda las propiedades del sistema numérico.
        \item Las operaciones en la estructura de datos se modelan a partir de sus contrapartes numéricas
        \begin{itemize}
            \item Agregar un objeto a la estructura de tamaño $n$ corresponde a la operación $n+1$
            \item Eliminar un objeto de la estructura de tamaño $n$ corresponde a la operación $n-1$
            \item Unir dos estructuras de tamaños $n,m$ corresponde a la operación $n+m$
        \end{itemize}
    \end{itemize}
\end{frame}

\section{Listas de acceso aleatorio}

\frame{
    \frametitle{Listas de acceso aleatorio}
    \begin{itemize}

    \item<1 ->
    Lista de dígitos con árboles binarios perfectos con rango ascendente.
    \item<2 ->
    Los elementos de la lista están en las hojas.
    \item<3 ->
    \begin{tikzpicture}

    \node[blue] at (0,1.5) {\Large 1};
    \node[blue] at (2,1.5) {\Large 1};
    \node[blue] at (4,1.5) {\Large 0};
    \node[blue] at (6,1.5) {\Large 1};

    \tikzstyle{level 1}=[sibling distance=1cm, level distance=0.7cm] 
    \node[circle, draw] at (0,0.7){1};
    \node[circle, fill] at (2, 1){}
      child{ node[circle, draw]{2}}
      child{ node[circle, draw]{3}};

    \tikzstyle{level 1}=[sibling distance=4cm, level distance=0.7cm] 
    \tikzstyle{level 2}=[sibling distance=2cm, level distance=0.5cm] 
    \tikzstyle{level 3}=[sibling distance=1cm, level distance=0.5cm]
    \tikzstyle{level 4}=[sibling distance=8mm, level distance=0.5cm]

    \node[anchor = north, circle, fill] at (6, 1) {}
      child{ node[anchor = north, circle, fill]{}
        child{ node[anchor = north, circle, fill]{}
          child{node[anchor = north, circle, draw]{4}}
          child{node[anchor = north, circle, draw]{5}}
        }
        child{ node[anchor = north, circle, fill]{}
          child{node[anchor = north, circle, draw]{6}}
          child{node[anchor = north, circle, draw]{7}}
        }
      }
      child{ node[anchor = north, circle, fill]{}
        child{ node[anchor = north, circle, fill]{}
          child{node[anchor = north, circle, draw]{8}}
          child{node[anchor = north, circle, draw]{9}}
        }
        child{ node[anchor = north, circle, fill]{}
          child{node[anchor = north, circle, draw]{10}}
          child{node[anchor = north, circle, draw]{11}}
        }
      };
    \end{tikzpicture}

    \end{itemize}
}

\begin{frame}
    \frametitle{Definición}
    Una lista de acceso aleatorio es una representación numérica, donde cada dígito distinto de cero tiene asociado un árbol binario perfecto de rango $i$, donde $i$ es la posición del dígito.
    \begin{align*}
        \texttt{RAL a} &= \texttt{Nil}\\
        &|\ \texttt{Zero (RAL a)}\\
        &|\ \texttt{One (Tree a) (RAL a)}
    \end{align*}
\end{frame}

\begin{frame}[fragile]
    \begin{lstlisting}
    consTree :: Tree a -> RAL a -> RAL a
    consTree t Nil = Cons (One t) Nil
    consTree t (Cons Zero ts) = Cons (One t) ts
    consTree t1 (Cons (One t2) ts) = Cons Zero (consTree (link t1 t2) ts)
    
    lookup :: RAL a -> Int -> a
    lookup (Cons  Zero   ts) i = lookup ts i
    lookup (Cons (One t) ts) i | i < size t = lookupTree t i
                             | otherwise  = lookup ts (i - size t)
    \end{lstlisting}
\end{frame}

\begin{frame}
    \frametitle{Desventajas hasta ahora}
    \begin{itemize}
        \item<1 ->
        Añadir y quitar elementos es $O(\log n)$. Las listas comunes realizan estas operaciones en $O(1)$
        \item<2 ->
        Una lista de acceso aleatorio, en teoría tiene un árbol binario perfecto de rango $i$ asociado al dígito en la posición $i$. No obstante, si esto no se cumple, en la implementación seguimos teniendo una lista de acceso aleatorio.
        \item<3 ->
        Más aún, el árbol asociado a cualquier dígito ¡no necesariamente es perfecto!
    \end{itemize}
\end{frame}


\begin{frame}
    \frametitle{Sistema numérico binario sesgado}
    El sistema numérico binario sesgado es un sistema numérico posicional donde:
    \begin{itemize}
    \item
    $D=\{0,\, 1,\, 2\}$
    \item
    $W=\{ 2^{i+1}-1 \,|\, i \in \mathbb{N} \}$
    \end{itemize}
    Esta representación no es única. No obstante, si limitamos la aparición del dígito $2$ a una única aparición y éste solo pueda ser el primer dígito distinto de cero, obtenemos la \emph{representación canónica}. La cual es única.

    Ejemplo:
    \begin{itemize}
    \item
    $20 \approx 2101$
    \end{itemize}
\end{frame}


\begin{frame}
    \frametitle{Implementación de LAA binaria sesgada}
    \begin{itemize}
    \item<1 ->
    Este sistema utiliza pesos de la forma $2^{r+1}-1$ que equivale al número de nodos internos de un árbol binario perfecto. Así que utilizaremos árboles con información en los nodos.
    \item<2 ->
    Nos interesará acceder al primer dígito no nulo en $O(1)$. Por ello, no haremos explícitos los dígitos nulos.
    \item<3 ->
    Al no utilizar dígitos nulos, requeriremos una etiqueta que nos diga en que posición se encontraría cada dígito si los dígitos nulos fueras explícitos.
    \item<4 ->
    Con esto, las operaciones de incremento y decremento se realizan en $O(1)$.
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \begin{lstlisting}

data RAL a = Nil
           | Cons (Int, Tree a) (RAL a)

cons x ts@(Cons (w1,t1) (Cons (w2,t2) ts')) 
    | w1 == w2 = Cons (1+w1+w2, (Node t1 x t2)) ts'
    | otherwise = Cons (1, Leaf x) ts
cons x l = Cons (1, Leaf x) l

lookup (Cons (w, t) ts) i
    | i < w = lookupTree w t i
    | otherwise = lookup ts (i-w)
    \end{lstlisting}
\end{frame}


\section{Tipos anidados}

\begin{frame}
    \frametitle{Definición}
    Un tipo anidado es un tipo recursivo cuyo parámetro de recursión es distinto al parámetro original
    \begin{align*}
    \texttt{Perfect a} & = \texttt{Zero a}\\
    &\ |\ \texttt{Succ (Perfect (a,a))}
    \end{align*}
\end{frame}

\begin{frame}[fragile]
    \texttt{S ( S ( S ( Z ((0,1),(2,3), ((4,5),(6,7))) )))}
    \begin{center}
    \begin{tikzpicture}[level distance=1cm]

    \tikzstyle{level 1}=[sibling distance=4cm] 
    \tikzstyle{level 2}=[sibling distance=2cm] 
    \tikzstyle{level 3}=[sibling distance=1cm]
    \tikzstyle{level 4}=[sibling distance=8mm]

    \node[anchor = north, circle, draw, dashed]{,}
      child{ node[anchor = north, circle, draw, dashed]{,}
        child{ node[anchor = north, circle, draw, dashed]{,}
          child{node[anchor = north, circle, draw]{0}}
          child{node[anchor = north, circle, draw]{1}}
        }
        child{ node[anchor = north, circle, draw, dashed]{,}
          child{node[anchor = north, circle, draw]{2}}
          child{node[anchor = north, circle, draw]{3}}
        }
      }
      child{ node[anchor = north, circle, draw, dashed]{,}
        child{ node[anchor = north, circle, draw, dashed]{,}
          child{node[anchor = north, circle, draw]{4}}
          child{node[anchor = north, circle, draw]{5}}
        }
        child{ node[anchor = north, circle, draw, dashed]{,}
          child{node[anchor = north, circle, draw]{6}}
          child{node[anchor = north, circle, draw]{7}}
        }
      };
    \end{tikzpicture}
    \end{center}
\end{frame}

\begin{frame}[fragile]
    \frametitle{LAA con tipos de dato anidados}
    \lstinputlisting[firstline=3, lastline=5]{src/NestedRALOkasaki.hs}
    \lstinputlisting[firstline=13, lastline=16]{src/NestedRALOkasaki.hs}
    \lstinputlisting[firstline=34, lastline=38]{src/NestedRALOkasaki.hs}
\end{frame}

\begin{frame}
    \frametitle{Tipos de dato anidados de orden superior}
    
    Un tipo de dato de orden superior es un tipo anidado donde la recursión es hecha sobre un constructor de tipos en vez de un tipo.

    % \begin{itemize}
    % \item
    % En nuestro ejemplo anterior, la recursión se hace con $(a,a)$, que es un tipo.
    % \end{itemize} 
    \lstinputlisting[firstline=1, lastline=3]{src/NestedRAL.hs}
    
    \end{frame}

\begin{frame}
    \frametitle{LAA con TDA de orden superior}
    Implementamos las LAA con sistema numérico $\{1,\,2\}$
    \lstinputlisting[firstline=4, lastline=8]{src/NestedRAL.hs}
\end{frame}

\begin{frame}
    \frametitle{Incremento}
    \begin{itemize}
    \item
    Idéntica a nuestra versión anterior
    \end{itemize}
    \lstinputlisting[firstline=24, lastline=30]{src/NestedRAL.hs}
\end{frame}

\begin{frame}
    \frametitle{Decremento}
    \begin{itemize}
    \item<1 ->
    Permitimos un \texttt{Zero} temporal
    \end{itemize}
    \lstinputlisting[firstline=35, lastline=43]{src/NestedRAL.hs}
\end{frame}

\begin{frame}
    \frametitle{Conclusiones}
    \begin{itemize}
    \item<1 ->
    Implementamos una estructura de datos que nos permite la misma flexibilidad que una lista común en cuanto añadir y quitar elementos.
    \item<2 ->
    Dicha estructura también realiza de manera eficiente el acceso y la actualización de elementos.
    \item<3 ->
    Además, esta estructura tiene una analogía con un sistema numérico. Lo cual, facilita la implementación de sus operaciones básicas.
    \item<4 ->
    Implementamos también, una estructura segura bajo tipado, es decir, no hay manera de crear una estructura inválida.
    \end{itemize}
\end{frame}

\end{document}