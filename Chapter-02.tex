\chapter{Listas de acceso aleatorio}

% Como se mencionó anteriormente, las listas comunes tienen la desventaja de no poder acceder a un elemento aleatorio de la lista de manera eficiente. Esto se debe a que un elemento sólo tiene referencia al siguiente, lo que ocasiona que para acceder a un elemento se deban recorrer todos los elementos anteriores. 

% Una \emph{lista de acceso aleatorio} es una estructura cuya finalidad es equivalente a las listas comunes. Nos interesa definir las operaciones \texttt{cons}, \texttt{head} y \texttt{tail} conservando el $O(1)$ que tenemos en listas comunes. Además estas listas se llamarán de \emph{acceso aleatorio} porque podremos acceder a cualquier elemento de la lista de manera eficiente. En general esta operación se realizará en $O(\log n)$.

En los capítulos anteriores hemos resuelto el problema del acceso aleatorio de manera teórica utilizando árboles binarios perfectos. Una vez que contamos con el concepto de representación numérica, podemos utilizarlo para obtener implementaciones de listas de acceso aleatorio. En adelante llamaremos \texttt{RAL} al tipo de listas de acceso aleatorio por sus siglas en inglés \emph{Random Access Lists}.

Nuestro principal interés está en definir la siguiente biblioteca de operaciones para listas de acceso aleatorio:

\begin{quote}

\begin{lstlisting}
-- Agregar un elemento al principio de una lista.
cons :: a -> RAL a -> RAL a

-- Separar una lista en cabeza y cola
uncons :: RAL a -> Maybe (a, RAL a)

-- Acceder a un elemento de la lista
lookup :: RAL a -> Int -> a

-- Actualizar un elemento de la lista
update :: RAL a -> Int -> a -> RAL a

-- Obtener una lista de acceso aleatorio a traves de una lista comun
fromList :: [a] -> RAL a

-- Obtener una lista comun a traves de una lista de acceso aleatorio
toList :: RAL a -> [a]

\end{lstlisting}

\end{quote}

A lo largo de este documento, nos referiremos a las listas de acceso aleatorio como LAA, o con su nombre completo de manera indistinta.

%% REVISADO 13 Febrero 2015

\section{Implementación binaria estándar}

Como primera implementación utilizaremos una representación numérica basada en el sistema binario estándar $B=\langle D,W \rangle$ con $D=\{0, 1\}$ y $W=\{1, 2, 4, 8\dots \}$.

Como ya se dijo, una estructura que guarda $n$ elementos se diseñará de acuerdo a la representación del número $n$ en binario. Por ejemplo, si queremos guardar 11 elementos, debemos diseñar una estructura a partir del número binario $1101$. Esto lo lograremos asociando a cada dígito $d_i$ un árbol binario perfecto de rango $i$ que solo guarda información en las hojas. Esto es conveniente puesto que el número de hojas en un árbol binario perfecto de rango $i$ es precisamente $2^i$. Siguiendo esta idea, la lista de los números del 1 al 11 se vería de la siguiente manera:

\begin{center}
\begin{tikzpicture}

\node[blue] at (0,1.5) {\Large 1};
\node[blue] at (2,1.5) {\Large 1};
\node[blue] at (4,1.5) {\Large 0};
\node[blue] at (6,1.5) {\Large 1};

\node[circle, draw] at (0,0.7){1};
\node[circle, fill] at (2, 1){}
  child{ node[circle, draw]{2}}
  child{ node[circle, draw]{3}};

\tikzstyle{level 1}=[sibling distance=4cm] 
\tikzstyle{level 2}=[sibling distance=2cm] 
\tikzstyle{level 3}=[sibling distance=1cm]
\tikzstyle{level 4}=[sibling distance=8mm]

\node[anchor = north, circle, fill] at (6, 1) {}
  child{ node[anchor = north, circle, fill]{}
    child{ node[anchor = north, circle, fill]{}
      child{node[anchor = north, circle, draw]{4}}
      child{node[anchor = north, circle, draw]{5}}
    }
    child{ node[anchor = north, circle, fill]{}
      child{node[anchor = north, circle, draw]{6}}
      child{node[anchor = north, circle, draw]{7}}
    }
  }
  child{ node[anchor = north, circle, fill]{}
    child{ node[anchor = north, circle, fill]{}
      child{node[anchor = north, circle, draw]{8}}
      child{node[anchor = north, circle, draw]{9}}
    }
    child{ node[anchor = north, circle, fill]{}
      child{node[anchor = north, circle, draw]{10}}
      child{node[anchor = north, circle, draw]{11}}
    }
  };
\end{tikzpicture}
\end{center}

La definición formal en Haskell es:
\begin{lstlisting}

data Digit a= One (Tree a)
  | Zero

data RAL a = [Digit a]

\end{lstlisting}

Los árboles se guardan en rango ascendente y el orden de los elementos es de izquierda a derecha, siendo la cabeza de la lista el hijo más a la izquierda del primer árbol. Notemos que el tamaño máximo de árboles en una lista de acceso aleatorio de tamaño $n$ es $\floor*{\log(n+1)}$ y el rango máximo de cualquier árbol es $\floor{log(n)}$.

Como mencionamos anteriormente, al ser las listas de acceso aleatorio una representación numérica, las operaciones de añadir y quitar elementos serán análogas a las operaciones sucesor y predecesor de un número binario. Recordemos ambas:

\begin{lstlisting}

inc [] = [One]
inc (Zero : ds) = One : ds
inc (One : ds) = Zero : (inc ds)

dec [One] = []
dec (One : ds) = Zero : ds
dec (Zero : ds) = One : (dec ds)

\end{lstlisting}

Para añadir un nuevo elemento al principio de la lista, la idea general es convertir el nuevo elemento en una hoja e insertar este nuevo árbol a la lista de árboles manteniendo los invariantes que mencionamos antes. 

\begin{lstlisting}

cons :: a -> RAL a -> RAL a
cons x l = consTree (Leaf x) l

consTree :: Tree a -> RAL a -> RAL a
consTree t [] = [One t]
consTree t (Zero : ts) = (One t) : ts
consTree t ((One t') : ts) = Zero : (consTree (link t t') ts)

\end{lstlisting}

Podemos observar nuevamente la analogía entre la función \texttt{inc} y la función \texttt{consTree}. La diferencia únicamente radica en el caso recursivo donde, además de la recursión, debemos ligar dos árboles binarios de rango $r$ para poder mantener el invariante de tener árboles de rango $i$ en la posición $i$. Para ello utilizamos la función \texttt{link}, cuya implementación es inmediata.


Para definir la función \texttt{uncons} requerimos primero una función más general \texttt{unconsTree} que dada una lista cuyo primer dígito tenga peso $r$, devolverá un par con un árbol de rango $r$ y la lista resultante de eliminar ese árbol en la anterior, la cual debe mantener el invariante de ser una lista de árboles perfectos con rangos ascendentes. Esta función es en la que se basa la función decremento.

\begin{lstlisting}
unconsTree :: RAL a -> Maybe (Tree a, RAL a)
unconsTree [] = None
unconsTree [One t] = Just (t, [])
unconsTree ((One t) : ts) = Just (t, Zero : ts)
unconsTree (Zero : ts) = let (Node t1 t2, ts') = unconsTree ts
  in (t1, (One t2) : ts')
\end{lstlisting}

Obsérvese que en el caso de tener una lista iniciando con \texttt{Zero}, el resultado del caso recursivo es siempre algo de la forma \texttt{(Node t1 t2, ts')}, i.e., la cabeza de una lista en el caso recursivo no puede ser una hoja. Ésto se debe a que ésta función considera como cabeza el primer árbol de la sucesión y debido al invariante de tener un árbol de rango $i$ en la posición $i$, después del primer \texttt{Zero} únicamente existen árboles de rango $r > 0$.

La definición de \texttt{uncons} se obtiene de manera sencilla aplicando \texttt{unconsTree} para obtener un árbol de rango 0, es decir, una hoja, y la lista resultante de quitarla.

\begin{lstlisting} 
uncons :: RAL a -> Maybe (a, RAL a)
uncons ts = let (Leaf x, ts') = unconsTree ts in (x, ts') 

\end{lstlisting}

Ambas operaciones ocupan una operación por dígito de la lista. Por lo tanto, su tiempo de ejecución es $O(\log(n))$ ya que existen a lo más $\floor{\log(n+1)}$ árboles.

Las operaciones \texttt{head} y \texttt{tail} se obtienen como la primera y segunda proyección de la función \texttt{uncons}.

Las operaciones de acceso ($lookup$ y $update$) no tienen una equivalencia con alguna operación binaria. No obstante, podemos implementarlas de manera eficiente aprovechando la estructura que hemos definido. Ambas operaciones son implementadas en dos pasos: primero buscamos el árbol necesario, posteriormente recorremos este árbol hasta llegar al elemento deseado.

\begin{lstlisting}

lookup :: Int -> RAL a -> a
lookup i (Zero : ts) = lookup i ts
lookup i ((One t) : ts)
  | i < size t = lookupTree i t
  | otherwise = lookup (i - size t) ts
  
lookupTree :: Int -> Tree a -> a
lookupTree 0 (Leaf x) = x
lookupTree i (Node t1 t2)
  | i < (size t) `div` 2 = lookupTree i t1
  | otherwise = lookupTree (i - ((size t) `div` 2)) t2
\end{lstlisting}

Se observa que la función \texttt{lookupTree} realiza una búsqueda binaria usual sobre los índices de la lista. La función \texttt{update} se hace de manera análoga.

\begin{lstlisting}
  
update :: Int -> a -> RAL a -> RAL a
update i y (Zero : ts) = update i y ts
update i y ((One t) : ts)
  | i < size t = (One (updateTree i y t)) : ts
  | otherwise = (One t) : (update (i - size t) y ts)
  
updateTree :: Int -> a -> Tree a -> Tree a
updateTree 0 y (Leaf x) = Leaf y
updateTree i y (Node t1 t2)
  | i < (size t) `div` 2 = Node (updateTree i y t1) t2
  | otherwise = Node t1 (updateTree (i - ((size t) `div` 2)) y t2)

\end{lstlisting}

Ambas operaciones recorren los dígitos hasta encontrar el árbol correcto, lo cual toma $O(\log n)$ y a lo más $O(\log n)$ para recorrer el árbol resultando en un total de $O(\log n)$ en el peor caso.

Finalmente, las funciones para convertir una lista convencional en una lista de acceso aleatorio y viceversa, las implementaremos de manera similar en todas las implementaciones vistas a lo largo de este documento. La primera transformación utiliza la operación de plegado sustituyendo el constructor $(:)$ por el constructor \texttt{cons}, es decir, nuestra implementación es:

\begin{lstlisting}

fromList :: [a] -> RAL a
fromList = foldr cons []

\end{lstlisting}

Hemos mencionado que las listas de acceso aleatorio guardan una analogía con el sistema numérico binario. La función \texttt{fromList} no queda fuera de esta analogía dado que su implementación se basa en llamadas sucesivas a la función \texttt{cons} que añade un elemento a la lista, análogamente a sumar 1 a un número binario. La función \texttt{toList} tampoco será la excepción, aunque la analogía es distinta. Dicha función es esencialmente la función de conversión del sistema binario al sistema unario. Puesto que, como ya mencionamos anteriormente, las listas convencionales se basan en el sistema numérico unario, donde el dígito 1 es representado con el constructor  \textsc{Cons} y el dígito 0 con el constructor \textsc{Nil}, restringiendo el uso del 0 únicamente para representar al 0 mismo. 

Empecemos analizando como transformar un número binario en unario. Por ejemplo, si consideramos el número binario 0101, que representa al número decimal 10, ¿cómo obtenemos exactamente 10 dígitos 1? Sabemos que la primera aparición del 1, en el ejemplo, tiene peso $2^1=2$ y su segunda aparición tiene peso $2^3=8$, sumando ambos pesos obtenemos el número de dígitos 1 que requerimos. La implementación en \textsc{Haskell} se torna un poco complicada porque hay que saber el peso de cada dígito 1 dentro de la sucesión de dígitos. Para ello, a la función de conversión agregamos un parámetro adicional que indicará el peso actual que representa cada dígito. Añadimos también la función \texttt{addSuccs} que será la encargada de añadir los constructores \texttt{Succ} necesarios. 

\begin{lstlisting}

data BinaryDigit = Zero | One
type Binary = [BinaryDigit]
data Unary = UZero | Succ (Unary)

toUnary :: Binary -> Int -> Unary
toUnary [] _ = UZero
toUnary (Zero:ts) weight = toUnary ts (weight*2)
toUnary (One:ts) weight = addSuccs weight (toUnary ts (weight*2)) 

addSuccs :: Int -> Unary -> Unary
addSuccs 0 u = u
addSuccs (n+1) u = Succ (addSuccs n u)

\end{lstlisting}

Aunque esta implementación se complicó más de lo deseado, nos daremos cuenta que siguiendo esta idea, la implementación de la conversión de listas de acceso aleatorio a listas comunes es mucho más sencilla. ¿La razón? Cada dígito sabe su peso a través de su árbol asociado. La analogía con nuestra función anterior es que cada dígito con peso $2^i$ debe agregar exactamente $2^i$ elementos a la estructura resultado. Esto es inmediato ya que cada árbol tiene $2^i$ elementos, que son los que agregaremos a la lista resultante. 

Analicemos la función \texttt{toUnary}. Si recibe una lista vacía, es decir, una sucesión vacía de dígitos, devuelve \texttt{ZeroU}. En nuestro objetivo final, este caso se traduce en convertir una sucesión vacía de dígitos en una lista común. Esa lista común resultado es la lista vacía \texttt{Nil}, que en \textsc{Haskell} es representada como $[]$.

El siguiente caso es convertir una sucesión de dígitos que inicia con \texttt{Zero}. En este caso, la función \texttt{toUnary} aplica la recursión sobre la cola pero con el doble del peso. Este último detalle se debe a que debemos multiplicar por 2 el resultado, esto es para recordar cuantos dígitos hemos recorrido y así saber qué peso tienen los dígitos en la llamada recursiva. En nuestro objetivo final, este detalle no es necesario dado que se puede saber el peso de cada dígito \texttt{One} a través de su árbol asociado.

El último caso es transformar una sucesión de dígitos que inicia con \texttt{One}. Observemos que la función \texttt{toUnary} hace uso de la función \texttt{addSuccs} para añadir $weight$ constructores \texttt{Succ}. Para nuestro objetivo final necesitaremos una función que añada $weight$ constructores \texttt{Cons}. ¿Cuál es esta operación? Esta operación será la concatenación. Se le concatenará una lista de $weight$ elementos al resultado de aplicar recursión al resto de la sucesión. Para ello añadiremos los elementos de un árbol a la estructura resultante, \emph{aplanando} el árbol asociado al dígito \texttt{One} que estamos revisando. El concepto de \emph{aplanado} se refiere a convertir el árbol en una lista que contiene sus elementos en el mismo orden. Así la implementación será:

\begin{lstlisting}

flat :: Tree a -> [a]
flat (Leaf x) = [x]
flat (Node t1 t2) = flat t1 ++ flat t2

toList :: RAL a -> [a]
toList [] = []
toList (Zero : ts) = toList ts
toList ((One t) : ts) = flat t ++ toList ts

\end{lstlisting}

Se observa que en comparación con la conversión anterior, en este caso, la definición se simplifica considerablemente puesto que el parámetro de peso y la función que agrega sucesores ya no son necesarias.

A lo largo de este documento, las implementaciones de estas dos funciones podrían presentar ciertas variaciones para adaptarse a las distintas versiones de las LAA que presentaremos. No obstante, la idea general será la misma.

Antes de continuar, presentaremos brevemente otra manera de construir una lista de acceso aleatorio de tamaño $n$. Esto nos será útil cuando queramos implementar arreglos de tamaño específico. 

En la función \texttt{fromList} se añade elemento a elemento. Esto provoca que se haga una sucesión de llamadas a \texttt{cons}. ¿Podemos construir una LAA de manera más eficiente? Por supuesto, podemos reducir el problema de la construcción de la lista de tamaño $n$ al problema de obtener la representación de $n$ en el sistema binario. El proceso es como sigue:

\begin{itemize}

\item
Si $n=0$, se ha terminado el proceso.
\item
Si $n\neq 0$, se obtiene el residuo de la división $\frac{n}{2}$ y se coloca como el dígito menos significativo del resultado. El resto del resultado será la conversión del número $\floor{\frac{n}{2}}$ al sistema numérico binario.

\end{itemize}

Con este proceso podemos construir fácilmente una lista de acceso aleatorio de tamaño $n$, siempre y cuando, los elementos a guardar sean el mismo. La implementación es:

\begin{lstlisting}

replicate :: Int -> Tree a -> RAL a
replicate 0 _ = []
replicate n t = if n `mod` 2 == 1 then
                (One t):(replicate (n `div` 2) (Node t t))
                else
                Zero:(replicate (n `div` 2) (Node t t))

\end{lstlisting}

Notemos que el caso recursivo se hace con el elemento \texttt{Node t t}. Dado que la cola de la lista debe contener árboles de un rango más alto. Es por esta razón que se vuelve complicado implementar la función \texttt{fromList} con este método ya que a pesar de que conocemos que forma tendrá la estructura final, no sabemos exactamente que elementos de la lista original deben guardarse en que árboles.

Aunque la función \texttt{replicate} puede ser útil en algunos casos, no es el objetivo que estamos buscando. Queremos implementar la función \texttt{fromList}. El problema con \texttt{replicate} es que recibe un número y un elemento a replicar. Podríamos decir que \texttt{replicate} convierte un número $n$ en su representación decimal a un número en un sistema numérico binario. Este proceso es sencillo y su implementación es muy sencilla, es por ello que nos gustaría tener un proceso similar para convertir una lista común (una representación numérica en un sistema numérico unario) en una lista de acceso aleatorio (una representación numérica en un sistema numérico binario). La conversión del sistema numérico decimal al binario se usan las funciones \texttt{mod} y \texttt{div}. Estas funciones están implementadas para números enteros en un sistema numérico decimal. Su implementación en un sistema numérico unario también es simple. Para nuestro objetivo, queremos implementarlas directamente en la representación numérica, es decir, queremos implementarlas sobre listas comunes. Lo primero que debemos hacer es definir funciones análogas a \texttt{mod} y \texttt{div} pero que funcionen sobre listas convencionales.

La implementación de estas operaciones se facilitará si las juntamos en una única función, a la que llamaremos \texttt{modDiv2}. Esta función devolverá un par, cuyo primer elemento es el resultado de la operación \texttt{mod} y el segundo el resultado de la función \texttt{div}. Lo primero que definiremos es: ¿qué significa la división de una lista entre 2? Esto significará convertir una lista de longitud $n$ en una lista de longitud $\floor{\frac{n}{2}}$ pero que contenga exactamente los mismos elementos. Dividir una lista con esta restricción, es imposible en este punto de nuestro análisis, puesto que no hay manera de tomar una lista de elementos de tipo \texttt{a} y devolver una lista con los mismos elementos pero de longitud menor preservando el tipo. Es por ello que restringiremos esta función a recibir únicamente listas de árboles. Así, si tenemos una lista de árboles de \texttt{a}, al dividir entre 2, la lista resultante tendrá los mismos elementos de tipo \texttt{a} pero contenidos en árboles más grandes. Es decir, para reducir la longitud de la lista a la mitad, pegaremos los árboles de la lista por pares. Esto nos lleva a un problema: ¿qué pasa si la longitud de la lista es impar? habría un árbol que no podría ser pegado con otro, este árbol lo descartaremos del resultado de la función \texttt{div}, y ahora es cuando la función \texttt{mod} debe hacer acto de presencia. Este árbol sobrante significa que la lista tenía longitud impar, es decir, el resultado de la operación \texttt{mod} no es cero sino dicho árbol sobrante. Así, la función \texttt{mod} tiene dos posibles resultados: un dígito \texttt{Zero} o un dígito \texttt{One} con un árbol asociado. Convenimos también que cuando la lista tenga longitud impar, el árbol asociado al resultado de \texttt{mod} será el primer árbol de la lista. 

La implementación de \texttt{modDiv2} se hará recursivamente con la idea intuitiva siguiente: si la lista de árboles es vacía, el módulo es \texttt{Zero} y la lista resultante de \texttt{div} es vacía. En otro caso, tenemos dos posibilidades. Si el módulo de la cola es \texttt{Zero}, entonces el resultado final es módulo \texttt{One} con la cabeza de la lista original como árbol asociado y como lista resultante de la división, la misma del resultado recursivo. Si el módulo de la cola es \texttt{One} con un árbol asociado \texttt{t}, entonces el resultado final es módulo \texttt{Zero}, y como lista resultante de la división, la misma del caso recursivo, añadiendo el árbol resultante de pegar la cabeza de la lista original y \texttt{t}, el árbol asociado al dígito \texttt{One} devuelto por el caso recursivo.

Para su implementación en \textsc{Haskell}, debemos hacer uso de otros constructores para los dígitos \texttt{Zero} y \texttt{One}, ya que estos están reservados para las listas de acceso aleatorio. Así, la implementación final de \texttt{modDiv2} es:

\begin{lstlisting}

data DigitsAux a= ZeroAux | OneAux a

modDiv2 :: [Tree a] -> (DigitsAux (Tree a), [Tree a])
modDiv2 [] = (ZeroAux, [])
modDiv2 (t:xs) = case modDiv2 xs of
                 (ZeroAux, q) -> (One t, q)
                 (OneAux t', q) -> (Zero, (Node t t'):q)

\end{lstlisting}

Haciendo uso de esta función auxiliar, podemos implementar la función \texttt{fromList} de inmediato:

\begin{lstlisting}

fromList :: [a] -> RAL a
fromList l = fromListAux (map (\x->Leaf x) l)

fromListAux :: [Tree a] -> RAL a
fromListAux [] = []
fromListAux l = case modDiv2 l of
              (ZeroAux, q) = Zero : (fromList q)
              (OneAux t, q) = (One t) : (fromList q)

\end{lstlisting}

Esta implementación, además de ser más elegante, deja más clara la analogía con la conversión entre sistemas numéricos. 

Las listas de acceso aleatorio que hemos presentado resuelven parcialmente nuestro problema. Ya tenemos el acceso aleatorio pero existen algunas desventajas. Como por ejemplo, la función \texttt{cons} se ejecuta en $O(\log n)$ y en las listas convencionales se ejecuta en $O(n)$. Es por ello que en las siguientes secciones presentaremos un par de versiones alternativas a ésta.

%% REVISADO 16-Feb-15


\section{Implementación sin ceros}\label{laasc}

La primera desventaja que observamos en la implementación vista en la sección anterior es que las funciones $cons$ y $uncons$ se ejecutan en $O(\log n)$ en vez de $O(1)$.

Obtener la cabeza de una lista debe de hacerse a través de la función $uncons$ que obtiene el primer elemento y reconstruye la lista quitando ese elemento. Esto reduce las líneas de código y funciones a implementar pero desperdicia tiempo de ejecución construyendo una lista que será descartada si sólo se busca obtener la cabeza de la lista. Para mayor eficiencia, implementemos primero la función $head$. Un caso de $head$ que corre en $O(1)$ es cuando la lista comienza con el dígito $1$.
\begin{center}
\lstinline$head ( (One (Leaf x) ) : _) = x$
\end{center}
Al observar esto, nos gustaría que el primer dígito de la lista nunca fuese $0$. Actualmente nuestro conjunto de dígitos es $\{0, 1\}$. Sin embargo, esto no es una regla. Podemos cambiar de representación de números binarios como mejor nos convenga. Utilicemos una representación sin ceros; esto es, utilizaremos un conjunto de dígitos que no contenga al $0$ y el mismo conjunto de pesos. Utilizaremos el conjunto de dígitos $\{1, 2\}$. Así el número decimal 16 se representa como $2111$ en vez de $00001$. 
Adecuar nuestras listas al cambio de dígitos se hace de una manera fácil. Al dígito 1 le seguimos asociando un árbol y al dígito 2 le asociamos un par de árboles. De hecho, podríamos asociarle al dígito 2 un único árbol de rango $i+1$ donde $i$ es la posición del dígito pero por claridad le asociaremos 2 árboles de rango $i$.

La definición del tipo cambia de esta manera:
\begin{lstlisting}

data Digit a= One (Tree a)
  | Two (Tree a, Tree a)

data RAL a = [Digit a]

\end{lstlisting}
La implementación final de \texttt{head} es:
\begin{lstlisting}
head :: RAL a -> a
head ( (One (Leaf x)) : _) = x
head ( (Two (Leaf x, _)) : _) = x
\end{lstlisting}
Que claramente se ejecuta en $O(1)$ en todos los casos. Ahora, la definición de la función \texttt{tail} es:

\begin{lstlisting}
tail :: RAL a -> RAL a
tail ( (Two (t1, t2)) : xs) = (One t2) : xs
tail ( (One _) : xs) = let (Node t1 t2) = head xs in (Two (t1, t2)) : (tail xs)
\end{lstlisting}

Esta implementación de \texttt{tail}, además de que mantiene el mismo tiempo de ejecución que la versión anterior, conlleva más problemas. Esta implementación claramente falla cuando la lista no contiene ningún dígito 2, puesto que, al final de la ejecución intentará obtener la cola de la lista vacía, la cual dijimos que no está definida. Resolver este inconveniente nos llevaría a manejar más casos o definir la cola de la lista vacía. Eso nos resulta poco conveniente, dado que realmente no ganamos demasiado con este cambio. Es por ello que mostraremos otra versión de las listas de acceso aleatorio.

% \section{LAA + Evaluación perezosa }

% Retomando nuestra implementación original de las LAA con el sistema numérico estándar, la operación \texttt{cons} estaba definida como:

% \begin{lstlisting}

% cons :: a -> RAL a -> RAL a
% cons x l = consTree (Leaf x) l

% consTree :: Tree a -> RAL a -> RAL a
% consTree t [] = [One t]
% consTree t (Zero : ts) = (One t) : ts
% consTree t1 ((One t2) : ts) = Zero : (consTree (link t1 t2) ts)

% \end{lstlisting}

% \begin{theorem}
% Con esta definición, la función \texttt{cons} se ejecuta en tiempo $O(1)$ amortizado si es evaluada de manera perezosa.
% \end{theorem}
% \begin{proof}
% Demostraremos este hecho utilizando el método del banquero. Asignamos un crédito a cada dígito \texttt{Zero} y ningún crédito a los dígitos \texttt{One}. Supongamos que la lista comienza con $k$ dígitos \texttt{One} seguidos de un \texttt{Zero}. Después la función \texttt{consTree} convierte cada uno de los dígitos \texttt{One} en \texttt{Zero} y el dígito \texttt{Zero} en \texttt{One}. Guardamos un crédito por cada uno de estos pasos. Ahora los dígitos \texttt{Zero} tienen un crédito pero el dígito \texttt{One} tiene dos: uno que tenía antes de la operación por ser \texttt{Zero} y el que recien le asignamos. Gastar estos dos créditos reestablece el invariante. <<TODO>>
% \end{proof}

% Podemos mostrar de manera equivalente que la función \texttt{uncons} se ejecuta en $O(1)$ amortizado. Todo esto es posible porque ambas operaciones \texttt{cons} y \texttt{uncons} no se combinan. No obstante, si utilizamos ambas operaciones en conjunto, al menos una debe tener $O(\log n)$ amortizado. Consideremos una sucesión de incrementos y decrementos que afectan a una lista de longitud $2^k$ haciendo ciclar su longitud entre $2^k$ y $2^k-1$. En ese caso, cada operación afecta todos los dígitos de la lista, ejecutándose todas las operaciones en $O(n \log n)$.

% Aunque habíamos demostrado que cada operación toma $O(1)$ amortizado, al utilizarlas en conjunto ya no ¿Por qué? El problema radica en que para demostrar la cota de ejecución, requerimos invariantes crediticias opuestas. 

% Debemos clasificar los dígitos de nuestra lista como \emph{seguros} y \emph{peligrosos} de tal manera que cada que una operación llegue a un dígito \emph{seguro} se detenga ahí y una operación que llegue a un dígito \emph{peligroso} pueda continuar. Así, para garantizar que dos operaciones seguidas, en la misma posición, no avancen ambas al siguiente dígito; debemos garantizar que cada que una operación llegue a un dígito \emph{peligroso}, deje un dígito \emph{seguro} en su lugar. Con ello, la siguiente operación que llegue a ese dígito, se detendría ahí. Podemos probar formalmente que las operaciones corren $O(1)$ amortizado usando la invariante crediticia de mantener un crédito en un dígito \emph{seguro} y un dígito \emph{peligroso} no tiene créditos.

% Ahora bien, la función de incremento requiere que el dígito más grande sea \emph{peligroso} y la función de decremento requiere que el dígito más pequeño sea \emph{peligroso}. Para mantener ambas operaciones con la cota amortizada simultáneamente, requeriremos añadir un dígito para que sea el dígito \emph{seguro}. Por lo que cambiamos nuestro conjunto de dígitos por uno que resulta en un sistema \emph{redundante}, es decir, un número no tiene una representación única en el sistema. Podemos redefinir las listas de acceso aleatorio de la siguiente manera, con las operaciones de incremento y decremento:

% \begin{lstlisting}

% data Digit a = Zero 
%              | One (Tree a)
%              | Two (Tree a) (Tree a)

% type RAL a = [Digit a]


% consTree :: Tree a -> RAL a -> RAL a
% consTree t [] = [One t]
% consTree t (Zero : ds) = (One t) : ds
% consTree t ((One t2) : ds) = (Two t t2) : ds
% consTree t ((Two t1 t2) : ds) = One t : (consTree (link t1 t2) ds)

% unconsTree :: RAL a -> (Tree a, RAL a)
% unconsTree ((One t) : ds) = (t, Zero : ds)
% unconsTree ((Two t1 t2) : ds) = (t1, (One t2) : ds)
% unconsTree (Zero : ds) = 
%     let (Node t1 t2, ds') = unconsTree ds in
%       (t1, (One t2) : ds')
%     end

% \end{lstlisting}

% Con esta implementación, las funciones \texttt{consTree} y \texttt{unconsTree} se ejecutan en $O(1)$ amortizado. Para ver este hecho, notemos que por ejemplo incrementar una secuencia cuyos dígitos son $222222$ resulta en la secuencia $1111111$ en 7 pasos (Despreciando llamadas a funciones \texttt{size} y \texttt{link} que sabemos se ejecutan eficientemente). Sin embargo, decrementar esta secuencia resultante no devuelve la secuencia original $222222$, en su lugar, devuelve $0111111$ en tan solo 1 paso. Por lo tanto, alternar operaciones de incremento y decremento no resulta más un problema.  

\section{Implementación binaria sesgada}

Aunque resolvimos el problema del acceso a la cabeza de una lista, aún tenemos el problema de que las funciones de incremento y decremento se ejecutan en $O(\log n)$. Este problema lo resolverá el sistema numérico binario sesgado descrito anteriormente.

Hemos mostrado las operaciones para incrementar y decrementar números binarios sesgados. Nuestro objetivo actual es utilizar esta representación para diseñar listas de acceso aleatorio. Dado que utilizaremos una implementación dispersa, definiremos las listas de acceso aleatorio como una lista de pares de enteros y árboles, los cuales pensamos como árboles binarios perfectos, de tal forma que el entero representa el rango del árbol. Más aún, los árboles serán guardados en rango ascendente y convenimos en que sólo los primeros dos árboles pueden ser del mismo rango. Esto corresponde a la propiedad del sistema binario segado acerca de que el dígito 2 sólo puede aparecer una vez y debe ser el primer dígito distinto de cero. Sabemos que los árboles perfectos tienen $2^i$ hojas pero el sistema sesgado requiere pesos de la forma $2^{i+1}-1$. Este problema se resuelve utilizando árboles perfectos con información en todos sus nodos, puesto que ya vimos que un árbol perfecto de rango $i$ tiene $2^{i+1}-1$ nodos.

En la sección anterior, también resolvimos el problema de acceder a la cabeza en $O(\log n)$ reduciéndolo a $O(1)$. Esta solución se vería estropeada si la cabeza sigue siendo el elemento más a la izquierda del árbol. Ahora guardamos datos en los nodos internos, por lo que optaremos por guardar la información del árbol en preorden en cada árbol. Así la cabeza de la lista será la raíz del primer árbol, recuperándose el tiempo de ejecución $O(1)$.

Así la nueva definición de nuestras LAA es:
\begin{lstlisting}

data Tree a = Leaf a
  | Node a (Tree a) (Tree a)

data RAL a = [(Int, Tree a)]
\end{lstlisting}

La operacion para agregar un elemento a la cabeza es:
\begin{lstlisting}

cons :: a -> RAL a -> RAL a
cons x ts@((w1, t1):(w2, t2): rest) 
  | w1 == w2 = (1+w1+w2, Node x t1 t2) : rest
  | otherwise = (1, Leaf x) : ts
cons x ts = (1, Leaf x) : ts
\end{lstlisting}

La implementación se explica a si misma recordando la definición de la función \texttt{succ} en la página \pageref{succ_skew}.

Las operaciones $head$ y $tail$ se implementan fácilmente puesto que la cabeza de la lista siempre está en la raíz del primer árbol. La implementación es:
\begin{lstlisting}

head :: RAL a -> a
head ((_, Leaf x) : _) = x
head ((_, Node x _ _) : _) = x
\end{lstlisting}

Por otra parte, la operación $tail$ también se implementa fácilmente siguiendo la correspondiente definición de la función \texttt{pred} de la página \pageref{pred_skew}.

\begin{lstlisting}
tail :: RAL a -> RAL a
tail ((_, Leaf _) : ts) = ts
tail ((w, Node _ t1 t2) : ts) = (w `div` 2, t1) : (w `div` 2, t2) : ts
\end{lstlisting}

%% REVISADO 18-Feb-15

Adecuar las operaciones de búsqueda y actualización es casi inmediato. Éstas recibirán también el tamaño del árbol para saber cuantos elementos tenemos en el árbol izquierdo y en el derecho.

\begin{lstlisting}

lookup :: Int -> RAL a -> a
lookup i ((w, t) : ts) 
  | i < w = lookupTree w i t
  | otherwise = lookup (i-w) ts

lookupTree :: Int -> Int -> Tree a -> a
lookupTree 1 0 (Leaf x) = x
lookupTree _ 0 (Node x _ _) = x
lookupTree w i (Node _ t1 t2)
  | i < w `div` 2 = lookupTree (w `div` 2) (i-1) t1
  | otherwise = lookupTree (w `div` 2) (i - 1 - (w `div` 2)) t2

\end{lstlisting}

En el caso recursivo, buscamos el elemento $i-1$ dado que ya descartamos la raíz del árbol original, a saber el elemento $x$. De manera similar, en la última línea ya hemos recorrido $1+\floor{\frac{w}{2}}$ elementos, que son los correspondientes a \texttt{t1} y a \texttt{x}, por lo que en el caso recursivo los restamos a \texttt{i}. 
La operación de actualización se define de manera análoga y su implementación se muestra en el apéndice este documento.

Es claro que las operaciones $cons$, $head$ y $tail$ se ejecutan en $O(1)$ y las operaciones $lookup$ y $update$ se ejecutan en $O(\log n)$

Las implementaciones vistas hasta ahora fueron realizadas fácilmente. Bastó razonar sobre la estructura siguiendo la analogía con el sistema numérico correspondiente obteniendo así la implementación deseada. Sin embargo, hasta ahora no nos hemos puesto a pensar sobre la pregunta ¿El tipo definido \texttt{RAL} realmente corresponde a la especificación dada en la teoría? La respuesta a esta pregunta es claramente ``no''.

En la teoría las listas de acceso aleatorio son sucesiones de dígitos donde cada dígito $d$ distinto de cero en la posición $i$ tiene asociados $d$ árboles binarios perfectos de rango $i$. Sin embargo, la implementación no obliga que los árboles utilizados sean en realidad árboles binarios perfectos. Puesto que el tipo \texttt{Tree a} genera árboles binarios cualesquiera, por ejemplo:

\begin{lstlisting}
Node (Node (Leaf 1) (Leaf 2)) (Leaf 5)
\end{lstlisting}

Es un árbol de tipo de \texttt{Tree Int} pero no es un árbol binario perfecto. Más aún, consideremos el siguiente elemento del tipo \texttt{RAL Int}

\begin{lstlisting}
[One (Leaf 5), One (Leaf 4), One (Leaf 3)]
\end{lstlisting}

¡También es una LAA válida bajo la definición del tipo! No obstante, está lejos de ser una lista de acceso aleatorio válida pues todos sus árboles son del mismo rango.

En conclusión, a pesar de que nuestra implementación fue bastante directa, las listas de acceso aleatorio son un subconjunto del tipo \texttt{RAL}. Una manera de resolver este problema es implementar una función \texttt{check} como lo hicimos en la implementación dispersa de los números binarios. No obstante, habría que utilizar esta función de verificación en todas las funciones que queramos definir. En su lugar, en el siguiente capítulo discutiremos la posibilidad de forzar las invariantes del tipo desde su propia definición utilizando conceptos avanzados de la programación funcional. 


Hemos definido las listas de acceso aleatorio y nos hemos dado cuenta que resultan ser una mejor alternativa que las listas comunes cuando de buscar y actualizar elementos aleatorios se trata. La implementación de las operaciones siguió la analogía con los sistemas numéricos, mostrando así, el beneficio de utilizar conceptos matemáticos para motivos prácticos de programación.

A pesar de las ventajas de las listas de acceso aleatorio, hay un problema latente que venimos arrastrando desde el capítulo anterior. El tipado no nos da ningún tipo de garantía sobre la estructura, por lo que el usuario podría alimentar nuestras funciones con entradas inválidas y el resultado sería erróneo. En el siguiente capítulo daremos la solución a este problema mostrando tipos que nos garantizarán invariantes de la estructura. Adaptaremos también nuestras listas de acceso aleatorio a este tipado.

