\chapter{Tipos de datos anidados}


Las implementaciones de las listas de acceso aleatorio vistas hasta ahora nos han permitido solucionar los inconvenientes con respecto al tiempo de ejecución y espacio de almacenamiento. Sin embargo, aun existe una gran desventaja. Nunca se garantiza que las estructuras cumplan los invariantes que hemos dado. Por ejemplo:

\begin{lstlisting}
Node (Node (Leaf 1) Empty) (Leaf 5)
\end{lstlisting}

Es un árbol válido bajo la definición del tipo. Sin embargo, no es un árbol binario perfecto, que son los árboles que requerimos para las listas de acceso aleatorio. De esta manera es posible construir elementos del tipo \texttt{RAL a} que no sean listas de acceso aleatorio de acuerdo a la definición.

Este problema se puede solucionar, como hemos discutido antes, con una función de verificación que decida si los árboles binarios involucrados en una lista de acceso aleatorio son realmente árboles perfectos. Esta función es:
\begin{lstlisting}

hg :: Tree a -> Int
hg (Leaf x) = 0
hg (Node t1 t2) = 1 + max (hg t1) (hg t2)

perfect :: Tree a -> Bool
perfect (Leaf x) = True
perfect (Node t1 t2) = perfect t1 &&
                       perfect t2
                       && hg t1 == hg t2
\end{lstlisting}

Sin embargo, ya se dijo que el uso de esta función es fuente de ineficiencia por lo que nos gustaría garantizar los invariantes de una estructura de datos de manera estática, por ejemplo, quisiéramos que todos los elementos del tipo \texttt{RAL a} sean listas de acceso aleatorio válidas. Para ello utilizaremos conceptos avanzados de la programación funcional. En particular, el concepto de tipos de datos anidados.

Un tipo de datos anidado es un tipo de datos recursivo cuyo parámetro es distinto al original en la llamada recursiva. Por ejemplo, consideremos la siguiente definición de árboles perfectos. Un árbol perfecto con etiquetas en \texttt{a} es una hoja etiquetada \texttt{Zero a} o bien un árbol perfecto con etiquetas en \texttt{(a,a)}. Esta definición corresponde a un tipo anidado, a saber:

\begin{lstlisting}
data Perfect a =  Zero a
                | Succ (Perfect (a,a))
\end{lstlisting}

Podemos observar que estamos definiendo el tipo \texttt{Perfect} sobre elementos de \texttt{a}. Curiosamente del lado derecho, el constructor \texttt{Succ} recibe un elemento de tipo \texttt{Perfect (a,a)}, por lo que la recursión no es uniforme.

La pregunta natural es ¿y eso para qué sirve? A simple vista la respuesta no es tan clara, así que analicemos algunos elementos de tipo \texttt{Perfect Int}:

\begin{itemize}
\item
\texttt{Zero 0}
\begin{center}
\begin{tikzpicture}[level distance=1cm]

\tikzstyle{level 1}=[sibling distance=4cm] 
\tikzstyle{level 2}=[sibling distance=2cm] 
\tikzstyle{level 3}=[sibling distance=1cm]
\tikzstyle{level 4}=[sibling distance=8mm]

\node[anchor = north, circle, draw]{0};
\end{tikzpicture}
\end{center}

\item
\texttt{Succ (Zero (0,1))}
\begin{center}
\begin{tikzpicture}[level distance=1cm]

\node[anchor = north, circle, draw, dashed]{,}
  child{ node[anchor = north, circle, draw]{0}}
  child{ node[anchor = north, circle, draw]{1}};
\end{tikzpicture}
\end{center}

\item
\texttt{Succ (Succ (Zero ((0,1),(2,3)))) }
\begin{center}
\begin{tikzpicture}[level distance=1cm]

\tikzstyle{level 1}=[sibling distance=4cm] 
\tikzstyle{level 2}=[sibling distance=2cm] 
\tikzstyle{level 3}=[sibling distance=1cm]
\tikzstyle{level 4}=[sibling distance=8mm]

\node[anchor = north, circle, draw, dashed]{,}
  child{ node[anchor = north, circle, draw, dashed]{,}
    child{ node[anchor = north, circle, draw]{0}
    }
    child{ node[anchor = north, circle, draw]{1}
    }
  }
  child{ node[anchor = north, circle, draw, dashed]{,}
    child{ node[anchor = north, circle, draw]{2}
    }
    child{ node[anchor = north, circle, draw]{3}
    }
  };
\end{tikzpicture}
\end{center}

\item
\texttt{Succ ( Succ ( Succ ( Zero ((0,1),(2,3), ((4,5),(6,7))) )))}
\begin{center}
\begin{tikzpicture}[level distance=1cm]

\tikzstyle{level 1}=[sibling distance=4cm] 
\tikzstyle{level 2}=[sibling distance=2cm] 
\tikzstyle{level 3}=[sibling distance=1cm]
\tikzstyle{level 4}=[sibling distance=8mm]

\node[anchor = north, circle, draw, dashed]{,}
  child{ node[anchor = north, circle, draw, dashed]{,}
    child{ node[anchor = north, circle, draw, dashed]{,}
      child{node[anchor = north, circle, draw]{0}}
      child{node[anchor = north, circle, draw]{1}}
    }
    child{ node[anchor = north, circle, draw, dashed]{,}
      child{node[anchor = north, circle, draw]{2}}
      child{node[anchor = north, circle, draw]{3}}
    }
  }
  child{ node[anchor = north, circle, draw, dashed]{,}
    child{ node[anchor = north, circle, draw, dashed]{,}
      child{node[anchor = north, circle, draw]{4}}
      child{node[anchor = north, circle, draw]{5}}
    }
    child{ node[anchor = north, circle, draw, dashed]{,}
      child{node[anchor = north, circle, draw]{6}}
      child{node[anchor = north, circle, draw]{7}}
    }
  };
\end{tikzpicture}
\end{center}

\end{itemize}

Hemos elegido poner una coma en los nodos internos de la representación gráfica de estos árboles para indicar la formación de tuplas de acuerdo a la siguiente transformación de un árbol binario común, es decir, de un elemento \texttt{Tree a}, a un elemento del tipo \texttt{Perfect a}:

\begin{itemize}
\item
Una hoja \texttt{Leaf x} se convierte en una hoja \texttt{Zero x}.
\item
Cada árbol común de la forma \texttt{Node (Leaf x) (Leaf y)} se convierte en una hoja de la forma \texttt{Zero (x,y)}.
\item
Se lleva la cuenta de cuantas veces se aplica la transformación. Esta cuenta será el número de constructores \texttt{Succ} que utilizaremos, ya que, un elemento de la forma \texttt{Zero (x, y)} tiene tipo \texttt{Perfect (a,a)} y queremos obtener un elemento de tipo \texttt{a}. Curiosamente, el número de \texttt{Succ} que utilicemos, será también el rango del árbol.
\end{itemize}

Si se aplica esta transformacion recursivamente, se puede transformar cualquier árbol binario perfecto común en un árbol de tipo \texttt{Perfect}. Es claro que si el árbol binario común no era perfecto, la transformación fallará, por lo que efectivamente, si $t$ es de tipo \texttt{Perfect a}, entonces necesariamente $t$ corresponde un árbol binario perfecto. Con lo cual se ha logrado nuestro objetivo de asegurar de manera estática el invariante de la definición de árboles perfectos.

Con esto hemos logrado definir árboles perfectos con datos en las hojas, pero ¿qué ocurre si queremos representar árboles con datos en los nodos? Una posible solución es añadir un tipo \texttt{Node} que representará los nodos del árbol:

\label{treeak}
\begin{lstlisting}

data Tree a k = Zero a
              | Succ (Tree (Node a k) k)

data Node a k = Node a k a

\end{lstlisting}

El tipo \texttt{Node a k} construye árboles binarios cuya raíz está etiquetada por \texttt{k} y sus subárboles son de tipo \texttt{a} (estamos pensando que el tipo $a$ es un tipo contenedor de elementos de $k$, i.e., el tipo \texttt{a} representa una estructura que puede guardar elementos de tipo \texttt{k}, por ejemplo, un árbol). En particular, el tipo \texttt{Node () k} construye hojas con elementos de $k$, puesto que en este caso, los subárboles siempre son vacíos ya que son elementos del tipo \texttt{()}. Así podemos construir árboles más complejos, por ejemplo:

\begin{center}

\begin{tikzpicture}[level distance=1cm]

\tikzstyle{level 1}=[sibling distance=2cm] 
\tikzstyle{level 2}=[sibling distance=1cm] 
\tikzstyle{level 3}=[sibling distance=5mm]

\node[anchor = north, circle, draw]{4}
  child{ node[anchor = north, circle, draw]{5}
    child{node[anchor = north, circle, draw]{6}}
    child{node[anchor = north, circle, draw]{7}}
  }
  child{ node[anchor = north, circle, draw]{8}
    child{node[anchor = north, circle, draw]{9}}
    child{node[anchor = north, circle, draw]{10}}
  };

\end{tikzpicture}
\end{center}

Obsérvese que si nos paramos en cualquier nodo de este árbol, el subárbol cuya raíz es dicho nodo se puede ver como instancia del tipo \texttt{Node a k}. Por ejemplo, el subárbol cuya raíz es 5, puede verse como un elemento del tipo \texttt{Node (Node () Int) Int}. Siguiendo este proceso de identificación de subárboles con elementos de algún tipo de la forma \texttt{Node a k} y contando el número de veces que se hizo esta identificación, mediante el uso de los constructores \texttt{Zero} y \texttt{Succ}, se obtiene un elemento del tipo anidado \texttt{Tree a k}. Los constructores \texttt{Zero} y \texttt{Succ} nos ayudarán a obtener fácilmente el rango del árbol. Por ejemplo, el árbol representado en la figura anterior se implementa como:

\begin{lstlisting}
Succ(
  Succ(
    Succ(
      Zero(
        Node(
          (Node
            (Node ()  6 ())
          5
            (Node ()  7 ())
          )
        4
          (Node
            (Node ()  9 ())
          8
            (Node () 10 ())
          )
        )
      )
    )
  )
)
\end{lstlisting}

Ahora que contamos con tipos de datos anidados para árboles binarios perfectos, ya es posible desarrollar las implementaciones de listas de acceso aleatorio, lo cual hacemos a continuación.
\section{LAA con tipos de datos anidados}

En la sección anterior construimos árboles binarios perfectos con datos en las hojas y en los nodos. Empezaremos adaptando nuestra primera implementación que utiliza árboles binarios perfectos con datos en las hojas. ¿Cómo adaptamos esta representación para representar listas de acceso aleatorio?

Veamos algunos ejemplos de manera intuitiva, apegándonos a la representación numérica, es decir, utilizando constructores \texttt{Zero} y \text{One}.

\begin{itemize}
\item
La lista vacía se representa con \texttt{Nil}.
\item
La lista $[1]$ se representa con \texttt{One 1 Nil}.
\item
La lista $[1,2]$ se representa como \texttt{Zero (One (1,2) Nil)}.
\item
La lista $[1,2,3]$ se representa como \texttt{One 1 (One (2,3) Nil)}.
\item
La lista $[1,2,3,4]$ se representa como \texttt{Zero (Zero (One ((1,2),(3,4)) Nil))}.
\item
La lista $[1,2,3,4,5]$ se representa como \texttt{One 1 (Zero (One ((2,3),(4,5)) Nil))}.
\end{itemize}

Se observa que el constructor \texttt{One} juega el papel del constructor \texttt{Cons}, es decir, agrega un elemento a la lista. Mientras que el constructor \texttt{Zero} encapsula listas de pares de \texttt{a} para considerarlas listas de elementos de \texttt{a}. La definición de listas de acceso aleatorio es la siguiente:

\lstinputlisting[firstline=3, lastline=5]{src/NestedRALOkasaki.hs}

Obsérvese que el anidamiento forza a que el constructor \texttt{One} añada $2^k$ elementos cada vez que se utiliza, esto no causa restricción alguna en la longitud de la lista debido a que todo número tiene una descomposición binaria. Más aún, como se ve en los ejemplos anteriores, la composición de los constructores \texttt{Zero} y \texttt{One} genera la representación binaria de la longitud de la lista.

Puesto que la implementación es esencialmente su representación binaria, volvemos a utilizar un razonamiento aritmético para nuestra implementación.

Las operaciones \texttt{cons} y \texttt{uncons} son idénticas a la versión sin tipos de datos anidados, a excepción de que separar y unir árboles ahora sólo se resume en separar y crear tuplas. Por lo tanto, las funciones \texttt{head} y \texttt{tail} también son idénticas.

\lstinputlisting[firstline=13, lastline=25]{src/NestedRALOkasaki.hs}

Pero detengámonos un momento a analizar la función \texttt{cons}. Ésta recibe listas de acceso aleatorio sobre \texttt{a} y un elemento de \texttt{a}, pero observando la llamada recursiva, podemos observar que \texttt{cons} que está recibiendo algo de tipo \texttt{(a, a)} y una lista de acceso aleatorio sobre \texttt{(a, a)}, es decir, ¡la llamada recursiva no tiene la misma firma que la llamada original! Cuando esto ocurre, diremos que la recursión es \emph{polimórfica} o \emph{no uniforme}. Utilizar tipos de datos anidados implica que muchas funciones utilizarán este tipo de recursión, por lo que se requerirá un razonamiento más complicado para construir la implementación. En particular, toda función recursiva polimórfica requiere la declaración de su firma, puesto que en otro caso, el intérprete de \textsc{Haskell} arrojará un error de unificación.

Las funciones \texttt{lookup} y \texttt{update} requieren un razonamiento diferente dado que la recursión ya no es uniforme. 

Para obtener un elemento en una lista que empieza con \texttt{One x xs} regresamos \texttt{x}s i buscamos el elemento en la posición 0, o volvemos a aplicar la función sin considerar \texttt{x}, es decir, sobre la misma secuencia sustituyendo \texttt{One} con \texttt{Zero} y buscando el elemento $i-1$ dado que acabamos de quitar un elemento.

Ahora bien, si la secuencia empieza con \texttt{Zero} obtendremos el par en la posición $\floor{\frac{i}{2}}$ y luego obtendremos el elemento correcto de ese par. Es fácil ver que todos los elementos con índice par están en los subárboles izquierdos. Si $i$ es par, entonces devolvemos el primer elemento de la tupla. Si no, devolvemos el segundo. 

\lstinputlisting[firstline=34, lastline=38]{src/NestedRALOkasaki.hs}

Finalmente, discutimos la operación para actualizar. Los casos cuando la lista empieza con el dígito \texttt{One} son sencillos: si el elemento a actualizar es el primero, simplemente cambiamos el elemento y dejamos la cola igual. Por otro lado, si requerimos actualizar el índice $i$ procedemos igual que con \texttt{lookup}, actualizamos el índice $i-1$ de la lista sustituyendo el primer dígito \texttt{One} por \texttt{Zero} y a este resultado le añadimos el elemento que tenía anteriormente el dígito \texttt{One}.

Si la lista inicia con \texttt{Zero} tenemos un problema porque requerimos actualizar el par en la posición $\floor{\frac{i}{2}}$ para lo cual necesitamos del otro elemento de la antigua tupla para construir la nueva. Para resolver esto, tenemos que hacer una llamada a la función \texttt{lookup}  en la posición $\floor{\frac{i}{2}}$, la cual devuelve un par que contiene al elemento original a actualizar. Este par se actualiza de manera adecuada en relación a la paridad del índice original y se utliza en la llamada recursiva a \texttt{update}. La implementación es como sigue:

\lstinputlisting[firstline=40, lastline=47]{src/NestedRALOkasaki.hs}\label{updateRALnested}

Para terminar esta implementación, hay que adaptar las funciones de conversión de una lista común a una lista de acceso aleatorio y viceversa. La primera se mantiene igual:

\lstinputlisting[firstline=27, lastline=28]{src/NestedRALOkasaki.hs}

Mientras que la segunda es un poco más compleja que antes, pero la idea es similar, repetimos varias veces la operación \texttt{uncons} hasta llegar a la lista de acceso aleatorio vacía.

\lstinputlisting[firstline=29, lastline=32]{src/NestedRALOkasaki.hs}

Esta no es la solución óptima pero es la más intuitiva. Para la versión mejorada de nuestras LAA con tipos de datos anidados, que discutiremos en la siguiente sección, mostraremos una versión mucho más eficiente para convertir a una lista común.

Con esto terminamos nuestra primera implementación con tipos anidados, la cual, utilizó una representación numérica binaria estándar. A continuación, desarrollamos la última implementación de este trabajo, utilizando la representación binaria sin ceros.

\section{LAA de orden superior}

La definición de árboles perfectos dada por el tipo anidado \texttt{Tree a k} discutida en la página \pageref{treeak} tiene una deficiencia importante, a saber, no existe relación alguna entre el tipo \texttt{k} y el tipo \texttt{a}. El hecho de que \texttt{a} sea un contenedor de elementos de \texttt{k} es de gran importancia pero no esta forzado en la definición del tipo, por lo que la implementación resultaría poco robusta. Resolvemos dicho inconveniente utilizando lo que se conoce como un \emph{tipo anidado de orden superior}, que es un tipo donde la recursión se hace sobre constructores de tipos en vez de tipos como tal. Empecemos dando una versión de orden superior del tipo de árboles binarios perfectos con datos en las hojas:

\begin{lstlisting}
data Perfect bush a = Zero (bush a)
                    | Succ (Perfect (Fork bush) a)

data Fork bush a = Fork (bush a) (bush a)
\end{lstlisting}

Como se observa, esta versión hace explícito que el primer argumento \texttt{bush} es un contenedor del segundo argumento \texttt{a}. Esto se nota al observer que utilizamos \texttt{bush a}, ya que esto forza a que \texttt{bush} tenga tipo \texttt{$* \rightarrow *$}, es decir, es un constructor de tipos. Más aún, el tipo \texttt{Fork} recibe como argumento el constructor de tipos \texttt{bush} (pensemos que \texttt{Fork} es un constructor de árboles) y un tipo \texttt{a} y devuelve un árbol obtenido pegando dos árboles del tipo \texttt{bush a}. Los elementos del tipo \texttt{Perfect bush a} son árboles construidos por \texttt{bush} encapsulados en el constructor \texttt{Zero}, o bien, árboles perfectos, donde el constructor \texttt{bush} se sustituyó por \texttt{Fork bush}, que es un constructor que pega dos árboles del tipo \texttt{bush a}.

A continuación utilizamos un tipo de datos similar a \texttt{Perfect} para implementar listas de acceso aleatorio, la cual se basa en la representación sin ceros, utilizando el sistema numérico binario con dígitos $\{1, 2\}$.

En la implementación anterior de las listas de acceso aleatorio sin ceros, discutida en la sección \ref{laasc} asociamos dos árboles de rango $i$ a cada dígito 2. No obstante, podemos observar que, en su lugar, podemos asociarle un único árbol de rango $i+1$. Hecha esta observación podemos definir las listas de acceso aleatorio como sigue:

\lstinputlisting[firstline=1, lastline=7]{src/NestedRAL.hs}

Utilizamos el tipo \texttt{HRAL} para dejar claro que éstas listas son LAA con tipo de datos de orden superior; adoptamos la \texttt{H} de las siglas en inglés de \emph{Higher order nested data types}. Las listas de acceso aleatorio que utilizará el usuario final se definen a partir de este tipo como:
\begin{lstlisting}
type RAL a = HRAL Leaf a
\end{lstlisting}

Empecemos a definir las funciones necesarias para la biblioteca de listas de acceso aleatorio. La función \texttt{cons} se hará utilizando una función auxiliar que llamaremos \texttt{incr} que dado un elemento de \texttt{Fork$^i$ Leaf} y una lista de acceso aleatorio sobre \texttt{Fork$^i$ Leaf} \footnotetext{\texttt{Fork$^i$} representa aplicar el constructor \texttt{Fork} $i$ veces} devuelve una lista de acceso aleatorio sobre \texttt{Fork$^i$ Leaf}. Las definiciones correspondientes son:

\lstinputlisting[firstline=24, lastline=30]{src/NestedRAL.hs}

Nuevamente, la función \texttt{incr} está basada en la función de incremento en el sistema numérico. Lo que sí resulta conveniente analizar es que significa tener una lista de acceso aleatorio sobre \texttt{Fork$^i$ Leaf}. Esto queda más claro al darnos cuenta que la función \texttt{incr} utiliza recursión polimórfica. La llamada recursiva del tercer caso tiene tipo \texttt{Fork bush a -> HRAL (Fork bush) a -> HRAL (Fork bush) a}, que es distinto al tipado declarado en la definición. Con esto podemos darnos cuenta que las listas de acceso aleatorio que utilizaremos comunmente, o las que usaría el usuario final, tienen tipo \texttt{HRAL Leaf a}. No obstante, al definir operaciones sobre estas listas, tendremos que trabajar con la cola de una lista con dicho tipo, y su cola tendrá tipo \texttt{HRAL (Fork Leaf) a} y a su vez su cola tendrá tipo \texttt{HRAL (Fork (Fork Leaf)) a}. A esto nos referiremos cuando digamos que tenemos listas de acceso aleatorio sobre \texttt{Fork$^i$ Leaf}; esto será equivalente a tener una lista con tipo \texttt{HRAL (Fork$^i$ Leaf) a}.

El algoritmo visto para decrementar un número binario presentado anteriormente es el siguiente:

\begin{lstlisting}

dec :: Nat -> Nat
dec [One] = []
dec (One :: ds) = Two :: (dec ds)
dec (Two :: ds) = One :: ds

\end{lstlisting}

Pero ahora mostraremos otra versión más elegante. Los primeros dos casos pueden ser simplificados si nos permitimos tener un único 0 de manera temporal y aplicando las siguientes reglas para quitarlo:

\begin{itemize}

\item
Eliminar el 0 de la secuencia cuyo único elemento es él, resulta en la secuencia vacía.
\item
Si la secuencia tiene más de dos elementos, nos fijamos en el dígito siguiente al único 0. Supongamos que el 0 está en la posición $i$ y procedemos como sigue:
\begin{itemize}
\item
Si la posición $i+1$ tiene al dígito 1, éste tiene asociado el peso $2^{i+1}$ y observemos que $2^{i+1}=2(2^i)$. Dicha está observación, quitamos el dígito 0, en la posición $i$, sustituyéndolo por un dígito 2. Además para preservar la suma de los pesos, el 1 en la posición $i+1$ se cambia por 0.
\item
Si la posición $i+1$ tiene al dígito 2, éste tiene asociado el peso $2^{i+1}$ y representa el valor $2(2^{i+1}) = 2^{i+1} + 2^{i+1} = 2(2^{i}) + 2^{i+1}$. Al ver esta igualdad, podemos darnos cuenta que el dígito 0 se debe eliminar sustituyéndolo por un 2 y cambiando el dígito 2 en la posición $i+1$ por un dígito 1.
\end{itemize}
\end{itemize}

Adaptando esta idea, recursivamente, obtenemos la siguiente definición para nuestras listas de acceso aleatorio:
\lstinputlisting[firstline=35, lastline=38]{src/NestedRAL.hs}

Con la ayuda de esta función, la separación en cabeza y cola es directa:
\lstinputlisting[firstline=40, lastline=43]{src/NestedRAL.hs}

Pasamos ahora a discutir las funciones de acceso. La idea para acceder a un elemento en una lista de acceso aleatorio, es la misma: Primero buscamos el árbol donde se encuentra el elemento y posteriormente buscamos el elemento dentro de dicho árbol. La función \texttt{accessHRAL} se encarga de buscar el árbol correspondiente, si la lista empieza con \texttt{One} y buscamos el elemento $0$, devolvemos el árbol asociado al primer dígito. En otro caso, buscamos el árbol en la posición $\floor{ \frac{i-1}{2} }$ en el resto de la lista, en dicha posición, necesariamente hay un árbol de la forma \texttt{Fork x y}. De acuerdo a la paridad de $i$ se devuelve uno de los árboles \texttt{x} o \texttt{y}

La idea es equivalente cuando la lista empieza con el dígito \texttt{Two}. Los elementos que pueden ser accedidos de inmediato son los elementos 0 y 1; los árboles asociados al dígito \texttt{Two}. En otro caso, buscamos en el resto de la lista, buscando el elemento $\floor{ \frac{i-2}{2} }$. El código correspondiente es:

\lstinputlisting[firstline=78, lastline=89]{src/NestedRAL.hs}

Para obtener la función de acceso para el tipo \texttt{RAL}, simplemente se invoca a la función \texttt{accessHRAL} con el constructor \texttt{Leaf}:

\lstinputlisting[firstline=90, lastline=92]{src/NestedRAL.hs}


La operación de actualización realizarse de manera análoga a la versión de las listas de acceso aleatorio basada en el sistema binario común, como se vio en la página \pageref{updateRALnested}. Sin embargo, queremos presentar una versión más elegante y eficiente.

En la versión anterior, debíamos hacer una llamada a la función de búsqueda para poder realizar la operación de actualización. Esto debía hacerse para poder construir el nuevo árbol que sustituiría a un árbol en la lista original. El nuevo árbol solo difería en una hoja. ¿Podemos evitar esa llamada a la función de búsqueda? La respuesta es sí, ¿cómo? construimos una función \emph{al vuelo} que recibirá un árbol y devolverá la versión modificada. Naturalmente el caso base de esta función será la sustitución de una hoja. El caso donde la función deberá cambiar es cuando se hace una llamada recursiva sobre la cola de la lista. En este caso, se actualizará el árbol izquierdo o derecho, según la paridad del índice que se reciba.

\lstinputlisting[firstline=94, lastline=104]{src/NestedRAL.hs}

La función de actualización para listas de acceso aleatorio se implementa fácilmente con una llamada a la función \texttt{fupdateHRAL} con la función que transforma cualquier hoja en la hoja cuya etiqueta es el elemento a actualizar.

\lstinputlisting[firstline=104, lastline=106]{src/NestedRAL.hs}

Por último, analicemos como convertir una lista de acceso aleatorio a una lista común. El proceso inverso será, como en todas las versiones anteriores, una instancia del operador \texttt{foldr}.

En nuestra versión anterior, mostramos un proceso intuitivo pero ineficiente para convertir una LAA con tipos de datos anidados a una lista común. Regresemos a la implementación que usamos con las LAA sin usar tipos de datos anidados: aplanar árboles y concatenarlos. Adaptando esta idea a los tipos de datos anidados obtenemos la siguiente definición:

\begin{lstlisting}

flatten (Leaf a) = [a]
flatten (Fork l r) = flatten l ++ flatten r

\end{lstlisting}

Sin embargo, esta función no está bien definida, puesto que, \texttt{Leaf a} y \texttt{Fork l r} no tienen el mismo tipo a pesar de que en conjunto representan árboles binarios perfectos. Además, como mencionamos antes, esta operación no es eficiente. 

Este inconveniente se corrije implementando una función para cada tipo:

\begin{lstlisting}

unleaf:: Leaf a -> [a]
unleaf (Leaf x) = [x]

unfork :: (bush a -> [a]) -> (Fork bush a -> [a])
unfork flatten (Fork l r) = flatten l ++ flatten r

\end{lstlisting}

La definición de \texttt{unfork} resulta más complicada de lo esperado debido al anidamiento de tipos. La idea detrás de su definición en un lenguaje más intuitivo es: Si sabemos como aplanar árboles de tipo \texttt{bush a} entonces podemos definir como aplanar árboles de tipo \texttt{Fork bush a}. De esta manera, la función polimórfica \texttt{unfork$^n$ unleaf} se encarga de aplanar árboles de tipo \texttt{Fork$^n$ Leaf a}.

Con esta misma idea podemos presentar la función \texttt{listify} que se sirve de \texttt{unfork} y aplana listas de acceso aleatorio.

\begin{lstlisting}

listify : (bush a -> [a]) -> (RAL bush a -> [a])
listify _ Nil = []
listify flatten (One b ds) = flatten b ++ listify (unfork flatten) ds
listify flatten (Two b ds) = unfork flatten b ++ listify (unfork flatten) ds

\end{lstlisting}

El primer argumento de la función \texttt{listify} es un ``aplanador'' de árboles de tipo \texttt{bush a}, el cual se actualiza en la llamada recursiva mediante la función \texttt{unfork}, para obtener un ``aplanador'' de árboles de tipo \texttt{Fork bush a}.

Nuevamente, la función \texttt{toList} para listas de acceso aleatorio es un caso particular de \texttt{listify} que utiliza la función \texttt{unleaf} como aplanador de hojas. Así la implementación final es:

\begin{lstlisting}

toList :: RAL a -> [a]
toList s = listify unleaf s

\end{lstlisting}

Las implementaciones de este capítulo, en particular, la implementación de \texttt{unfork} y \texttt{listify} dejan ver que una implementación con tipos de datos anidados, es más elaborada que una implementación convencional. Esto se debe a que un tipo de datos anidado, realmente está definiendo a una familia infinita de tipos, los cuales no son independientes, sino que, están entrelazados por su definición. Debido a esto las funciones que involucran a un tipo anidado, realmente son familias infinitas de funciones entrelazadas. Por ejemplo, la función \texttt{listify} para \texttt{HRAL bush a} utiliza en la llamada recursiva a la función \texttt{listify} para \texttt{HRAL (Fork bush) a}, la cual a su vez llama a la función \texttt{listify} para \texttt{HRAL (Fork (Fork bush)) a}, etc, etc. Esto implica en particular que las signaturas de esta clase de funciones sean complicadas puesto que requieren funciones auxiliares como el caso de \texttt{flatten} para la función \texttt{listify}. Dado que, cuando se usan tipos anidados, las signaturas son obligatorias es importante tratar de simplificarlas. Esto se puede hacer mediante el mecanismo de clases de \textsc{Haskell}. Ejemplificamos la idea para el caso de la función \texttt{listify}, lo cual resulta en una versión más elegante de dicha operación.

Considérese la clase \texttt{Flatten} definida como sigue:

\lstinputlisting[firstline=52, lastline=53]{src/NestedRAL.hs}

Se observa que esta es una clase de orden superior puesto que sus elementos son constructores de tipos y no tipos. Los constructores de la clase \texttt{Flatten} son aquellos constructores de árbol \texttt{bush} que tienen definida una función \texttt{flatten:: bush a ->[a]}, es decir, los constructores de árbol que soportan una función de aplanamiento.

Nuestros constructores de tipo \texttt{Leaf} y \texttt{Fork} se pueden instanciar a esta clase como sigue:

\lstinputlisting[firstline=55, lastline=59]{src/NestedRAL.hs}

Se observa que el constructor \texttt{Fork bush} solo puede pertenecer a la clase \texttt{flatten} si \texttt{bush} es miembro de la misma clase. Usando esta clase la función \texttt{listify} se simplifica como sigue:

\lstinputlisting[firstline=61, lastline=64]{src/NestedRAL.hs}

Podemos notar que el lado derecho es idéntico en ambos casos recursivos. Esto es porque la función \texttt{flatten} está sobrecargada debido al mecanismo de clases. En el primer caso su tipo es \texttt{bush a -> [a]} mientras que en el segundo caso tiene tipo \texttt{Fork bush a -> [a]}. También es importante notar que el trato uniforme de ambos casos se debe a que elegimos representar los dos árboles asociados al dígito \texttt{Two} con un único árbol. Ahora bien, la función \texttt{toList} se convierte en \texttt{listify}.

\lstinputlisting[firstline=66, lastline=67]{src/NestedRAL.hs}

Con esto terminamos nuestra implementación de listas de acceso aleatorio utilizando tipos de datos anidados, la cual, además de tener todas las ventajas mencionadas en el capítulo anterior, tiene la propiedad de garantizar estáticamente, es decir, mediante el tipado, que las listas manipuladas sean listas de acceso aleatorio válidas.