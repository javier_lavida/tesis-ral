
data Tree a k = Zero a
              | Succ (Tree (Node a k) k)

data Node a k = Node a k a


data Perfect bush a = ZeroP (bush a)
                    | SuccP (Perfect (Fork bush) a)

data Fork bush a = Fork (bush a) a (bush a)

data Empty a = Empty