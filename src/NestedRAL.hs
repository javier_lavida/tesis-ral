data Leaf a = Leaf a
data Fork bush a = Fork (bush a) (bush a)

data HRAL bush a =
       Nil
     | One (bush a) (HRAL (Fork bush) a)
     | Two (Fork bush a) (HRAL (Fork bush) a)
     
type RAL = HRAL Leaf
{--
instance (Show a) => Show (Leaf a) where
   show (Leaf i) = "Leaf " ++ (show i)

instance (Show bush, Show a) => Show (Fork bush a) where
   show (Fork bush a) = "Fork (" ++ (show bush) ++ ") (" ++ show

instance (Show bush, Show a) => Show (HRAL bush a) where
   show Nil = "nil"
   show (One t l) = "One (" ++ (show t) ++ ") (" ++ (show l) ++ ")"
   show (Two t l) = "Two (" ++ (show t) ++ ") (" ++ (show l) ++ ")"

--}

cons :: a -> RAL a -> RAL a
cons a s = incr (Leaf a) s

incr :: bush a -> HRAL bush a -> HRAL bush a
incr b Nil = One b Nil
incr b1 (One b2 ds) = Two (Fork b1 b2) ds
incr b1 (Two b2 ds) = One b1 (incr b2 ds)

fromList :: [a] -> RAL a
fromList = foldr cons Nil

zero :: HRAL (Fork bush) a -> HRAL bush a
zero Nil = Nil
zero (One b ds) = Two b (zero ds)
zero (Two (Fork b1 b2) ds) = Two b1 (One b2 ds)

uncons :: RAL a -> Maybe (a, RAL a)
uncons Nil = Nothing
uncons (One (Leaf a) ds) = Just (a, zero ds)
uncons (Two (Fork (Leaf a) b) ts) = Just (a, One b ts)

unleaf :: Leaf a -> [a]
unleaf (Leaf a) = [a]

unfork :: (bush a -> [a]) -> (Fork bush a -> [a])
unfork flatten (Fork l r) = flatten l ++ flatten r


class Flatten bush where
      flatten :: bush a -> [a]

instance Flatten Leaf where
      flatten (Leaf a) = [a]

instance (Flatten bush) => Flatten (Fork bush) where
      flatten (Fork l r) = flatten l ++ flatten r

listify :: (Flatten bush) => HRAL bush a -> [a]
listify Nil = []
listify (One b ds) = flatten b ++ listify ds
listify (Two b ds) = flatten b ++ listify ds

toList :: RAL a -> [a]
toList s = listify s

{--
lookupHRAL :: Integer -> RAL a -> a
lookupHRAL 0 (One (Leaf a) _) = a
lookupHRAL i (One t ts) = lookupHRAL (i-1) (zero ts)
lookupHRAL 0 (Two (Fork (Leaf a) _) _) = a
lookupHRAL 1 (Two (Fork _ (Leaf a)) ts) = a 
lookupHRAL i (Two _ ts) = lookupHRAL (i-2) (zero ts)
--}

lookupHRAL :: Integer -> HRAL bush a -> bush a
lookupHRAL 0 (One t _) = t
lookupHRAL i (One _ ts) = let
                           Fork x y = lookupHRAL ((i-1) `div` 2) ts
                         in
                           if ((i-1) `mod` 2) == 0 then x else y
lookupHRAL 0 (Two (Fork t1 _) _) = t1
lookupHRAL 1 (Two (Fork _ t2) _) = t2
lookupHRAL i (Two t ts) = let
                           Fork x y = lookupHRAL ((i-2) `div` 2) ts
                         in
                           if ((i-2) `mod` 2) == 0 then x else y

lookup :: Integer -> RAL a -> a
lookup i ral = let Leaf a = lookupHRAL i ral in a

fupdateHRAL :: (bush a -> bush a) -> Integer -> HRAL bush a -> HRAL bush a
fupdateHRAL f 0 (One t xs ) = One (f t) xs
fupdateHRAL f i (One t ts) = incr t (fupdateHRAL f (i-1) (zero ts))
fupdateHRAL f 0 (Two (Fork b1 b2) ts) = Two (Fork (f b1) b2) ts
fupdateHRAL f 1 (Two (Fork b1 b2) ts) = Two (Fork b1 (f b2)) ts
fupdateHRAL f i (Two t ts) =
           let
             f' = \(Fork x y) -> if ((i-2) `mod` 2) == 0 then Fork (f x) y else Fork x (f y)
           in
             Two t (fupdateHRAL f' ((i-2) `div` 2) ts)

update :: Integer -> a -> RAL a -> RAL a
update i a ral = fupdateHRAL (\(Leaf x) -> (Leaf a)) i ral