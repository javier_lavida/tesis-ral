{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleContexts #-}
module RALStandard where
import Prelude hiding (lookup, replicate)
import RandomAccessList


data Tree a = Leaf a
            | Node Int (Tree a) (Tree a)

data Digit a = Zero
            | One (Tree a)


data RAL a = Nil
           1| Cons (Digit a) (RAL a)


instance Show a => Show (Tree a) where
    show (Leaf x) = "Leaf " ++ (show x)
    show (Node _ t1 t2) = "Node (" ++ (show t1) ++ ", " ++ (show t2) ++")"
instance Show a => Show (Digit a) where
    show (Zero) = "Zero"
    show (One t) = "One (" ++ (show t) ++ ")"

instance Show a => Show (RAL a) where
    show l = show (tolst l)
        where
        tolst Nil = []
        tolst (Cons d l) = d : (tolst l) 


link :: Tree a -> Tree a -> Tree a
link (Leaf x1) (Leaf x2) = Node 1 (Leaf x1) (Leaf x2)
link t1@(Node n _ _) t2@(Node m _ _)
    | n == m = Node (n+1) t1 t2
    | otherwise = error "n != m"

size :: Tree a -> Int
size (Leaf _) = 1
size (Node n _ _) = 2^n

flat :: Tree a -> [a]
flat (Leaf x) = [x]
flat (Node _ t1 t2) = flat t1 ++ flat t2

consTree :: Tree a -> RAL a -> RAL a
consTree t Nil = Cons (One t) Nil
consTree t (Cons Zero ts) = Cons (One t) ts
consTree t1 (Cons (One t2) ts) = Cons Zero (consTree (link t1 t2) ts)

unconsTree :: RAL a -> (Tree a, RAL a)
unconsTree (Cons (One t) Nil) = (t, Nil)
unconsTree (Cons (One t) ts ) = (t, Cons Zero ts)
unconsTree (Cons   Zero  ts ) =
    let (Node _ t1 t2, ts') = unconsTree ts
    in (t1, Cons (One t2) ts')

lookupTree :: Tree a -> Int -> a
lookupTree (Leaf x) 0 = x
lookupTree t@(Node _ t1 t2) i 
    | i < (size t) `div` 2 = lookupTree t1 i
    | otherwise = lookupTree t2 (i - ((size t) `div` 2))
    
    
updateTree :: Tree a -> Int -> a -> Tree a
updateTree (Leaf x) 0 y = Leaf y
updateTree t@(Node n t1 t2) i y
    | i < (size t) `div` 2 = Node n (updateTree t1 i y) t2
    | otherwise = Node n t1 (updateTree t2 (i - ((size t) `div` 2)) y)


uncons :: RAL a -> (a, RAL a)
uncons ts = let (Leaf x, ts') = unconsTree ts in (x, ts') 

instance RandomAccessList.RALClass RAL a where

    --cons :: a -> RAL a -> RAL a
    cons x ts = consTree (Leaf x) ts

    head = fst.uncons
    tail = snd.uncons
    
    -- lookup :: RAL a -> Int -> a
    lookup (Cons  Zero   ts) i = lookup ts i
    lookup (Cons (One t) ts) i | i < size t = lookupTree t i
                               | otherwise  = lookup ts (i - size t)

    -- update :: RAL a -> Int -> a -> RAL a
    update (Cons Zero  ts) i y = update ts i y
    update (Cons (One t) ts) i y | i < size t = Cons (One (updateTree t i y)) ts
                                 | otherwise  = Cons (One t) (update ts (i - size t) y)

    
    --fromList :: [a] -> RAL a
    fromList = foldr cons Nil

    --toList :: RAL a -> [a]
    toList   Nil = []
    toList (Cons Zero ts) = toList ts
    toList (Cons (One t) ts) = (flat t) ++ toList ts


replicate :: Int -> Tree a -> RAL a
replicate 0 t = Nil
replicate n t@(Node r t1 t2) =
    if n `mod` 2 == 1 then
        Cons (One t) (replicate (n `div` 2) (Node (r+1) t t))
    else
        Cons  Zero  (replicate (n `div` 2) (Node (r+1) t t))


modDiv2 :: [Tree a] -> (Digit a, [Tree a])
modDiv2 [] = (Zero, [])
modDiv2 (t:ts) = case modDiv2 ts of
                                    (Zero, q) -> (One t, q)
                                    (One t', q) -> (Zero, (link t t'):q)

fromListV2 :: [a] -> RAL a
fromListV2 l = fromListAux (map (\x->Leaf x) l)

fromListAux :: [Tree a] -> RAL a
fromListAux [] = Nil
fromListAux l = Cons d (fromListAux ts)
                                where (d,ts) = modDiv2 l



