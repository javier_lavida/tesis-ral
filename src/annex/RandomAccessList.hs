{-# LANGUAGE MultiParamTypeClasses #-} 

module RandomAccessList where

import Prelude hiding (lookup)


class RALClass r a where
  cons :: a -> r a -> r a
  head :: r a -> a
  tail :: r a -> r a
  lookup :: r a -> Int -> a
  update :: r a -> Int -> a -> r a
  fromList :: [a] -> r a
  toList :: r a -> [a]