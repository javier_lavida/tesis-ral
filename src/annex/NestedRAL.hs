{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleContexts #-}
module RALNestedHighOrder where
import Prelude hiding (lookup)
import RandomAccessList

data Leaf a = Leaf a
data Fork bush a = Fork (bush a) (bush a)

data RAL bush a =
       Nil
     | One (bush a) (RAL (Fork bush) a)
     | Two (Fork bush a) (RAL (Fork bush) a)
     
type IxSequence = RAL Leaf

class Flatten bush where
      flatten :: bush a -> [a]

instance (Show a) => Show (Leaf a) where
   show (Leaf i) = "Leaf " ++ (show i)

instance (Show (bush a), Show a) => Show (Fork bush a) where
   show (Fork f1 f2) = "Fork [" ++ (show f1) ++ "," ++ show f2 ++ "]"

instance (Show (bush a), Show a) => Show (RAL bush a) where
   show Nil = "nil"
   show (One t l) = "One <" ++ (show t) ++ "> (" ++ (show l) ++ ")"
   show (Two t l) = "Two <" ++ (show t) ++ "> (" ++ (show l) ++ ")"

instance Flatten Leaf where
      flatten (Leaf a) = [a]

instance (Flatten bush) => Flatten (Fork bush) where
      flatten (Fork l r) = flatten l ++ flatten r

incr :: bush a -> RAL bush a -> RAL bush a
incr b Nil = One b Nil
incr b1 (One b2 ds) = Two (Fork b1 b2) ds
incr b1 (Two b2 ds) = One b1 (incr b2 ds)

zero :: RAL (Fork bush) a -> RAL bush a
zero Nil = Nil
zero (One b ds) = Two b (zero ds)
zero (Two (Fork b1 b2) ds) = Two b1 (One b2 ds)

front :: IxSequence a -> Maybe (a, IxSequence a)
front Nil = Nothing
front (One (Leaf a) ds) = Just (a, zero ds)
front (Two (Fork (Leaf a) b) ts) = Just (a, One b ts)

accessRAL :: Int -> RAL bush a -> bush a
accessRAL 0 (One t _) = t
accessRAL i (One _ ts) = let
                           Fork x y = accessRAL ((i-1) `div` 2) ts
                         in
                           if ((i-1) `mod` 2) == 0 then x else y
accessRAL 0 (Two (Fork t1 _) _) = t1
accessRAL 1 (Two (Fork _ t2) _) = t2
accessRAL i (Two t ts) = let
                           Fork x y = accessRAL ((i-2) `div` 2) ts
                         in
                           if ((i-2) `mod` 2) == 0 then x else y

fupdateRAL :: (bush a -> bush a) -> Int -> RAL bush a -> RAL bush a
fupdateRAL f 0 (One a xs ) = One (f a) xs
fupdateRAL f i (One t ts) = incr t (fupdateRAL f (i-1) (zero ts))
fupdateRAL f 0 (Two (Fork b1 b2) ts) = Two (Fork (f b1) b2) ts
fupdateRAL f 1 (Two (Fork b1 b2) ts) = Two (Fork b1 (f b2)) ts
fupdateRAL f i (Two t ts) =
           let
             f' = \(Fork x y) -> if ((i-2) `mod` 2) == 0 then Fork (f x) y else Fork x (f y)
           in
             Two t (fupdateRAL f' ((i-2) `div` 2) ts)


unleaf :: Leaf a -> [a]
unleaf (Leaf a) = [a]

unfork :: (bush a -> [a]) -> (Fork bush a -> [a])
unfork flatten (Fork l r) = flatten l ++ flatten r

listify :: (Flatten bush) => RAL bush a -> [a]
listify Nil = []
listify (One b ds) = flatten b ++ listify ds
listify (Two b ds) = flatten b ++ listify ds


instance RandomAccessList.RALClass IxSequence a where

  -- cons :: a -> IxSequence a -> IxSequence a
  cons a s = incr (Leaf a) s

  head l = case front l of
              Nothing -> error "Empty list"
              Just (x,_) -> x

  tail l = case front l of
              Nothing -> error "Empty list"
              Just (_,xs) -> xs

  -- lookup :: IxSequence a -> Int -> a
  lookup ral i = let Leaf a = accessRAL i ral in a

  -- update :: IxSequence a -> Int -> a -> IxSequence a
  update ral i a = fupdateRAL (\(Leaf x) -> (Leaf a)) i ral

  -- fromList :: [a] -> IxSequence a
  fromList = foldr cons Nil

  -- toList :: IxSequence a -> [a]
  toList s = listify s
