{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleContexts #-}
module RALOneTwo where
import Prelude hiding (lookup)
import RandomAccessList

data Tree a = Leaf a
            | Node Int (Tree a) (Tree a)

data Digit a = One (Tree a)
             | Two (Tree a) (Tree a)

data RAL a = Nil
           | Cons (Digit a) (RAL a)


instance Show a => Show (Tree a) where
  show (Leaf x) = "Leaf " ++ (show x)
  show (Node _ t1 t2) = "Node (" ++ (show t1) ++ ", " ++ (show t2) ++")"
instance Show a => Show (Digit a) where
  show (One t) = "One (" ++ (show t) ++ ")"
  show (Two t1 t2) = "Two <" ++ (show t1) ++ ", " ++ (show t2) ++">"

instance Show a => Show (RAL a) where
  show l = show (tolst l)
    where
    tolst Nil = []
    tolst (Cons d l) = d : (tolst l) 


link :: Tree a -> Tree a -> Tree a
link (Leaf x1) (Leaf x2) = Node 1 (Leaf x1) (Leaf x2)
link t1@(Node n _ _) t2@(Node m _ _) | n == m = Node (n+1) t1 t2
                             | otherwise = error "n != m"

size :: Tree a -> Int
size (Leaf _) = 1
size (Node n _ _) = 2^n

flat :: Tree a -> [a]
flat (Leaf x) = [x]
flat (Node _ t1 t2) = flat t1 ++ flat t2

consTree :: Tree a -> RAL a -> RAL a
consTree t Nil = Cons (One t) Nil
consTree t1 (Cons (One t2) ts) = Cons (Two t1 t2) ts
consTree t1 (Cons (Two t2 t3) ts) = Cons (One t1) (consTree (link t2 t3) ts)

unconsTree :: RAL a -> (Tree a, RAL a)
unconsTree (Cons (One t) Nil) = (t, Nil)
unconsTree (Cons (Two t1 t2) Nil) = (t1, Cons (One t2) Nil)
unconsTree (Cons (One t) ts) = let (Node _ t1 t2, ts') = unconsTree ts in (t, Cons (Two t1 t2) ts')
unconsTree (Cons (Two t1 t2) ts) = (t1, Cons (One t2) ts)

  
lookupTree :: Tree a -> Int -> a
lookupTree (Leaf x) 0 = x
lookupTree t@(Node _ t1 t2) i | i < (size t) `div` 2 = lookupTree t1 i
                              | otherwise = lookupTree t2 (i - ((size t) `div` 2))
  
  
updateTree :: Tree a -> Int -> a -> Tree a
updateTree (Leaf x) 0 y = Leaf y
updateTree t@(Node n t1 t2) i y
          | i < (size t) `div` 2 = Node n (updateTree t1 i y) t2
          | otherwise = Node n t1 (updateTree t2 (i - ((size t) `div` 2)) y)


uncons :: RAL a -> (a, RAL a)
uncons ts = let (Leaf x, ts') = unconsTree ts in (x, ts') 


instance RandomAccessList.RALClass RAL a where

  --cons :: a -> RAL a -> RAL a
  cons x ts = consTree (Leaf x) ts

  head = fst.uncons
  tail = snd.uncons
    
  -- lookup :: RAL a -> Int -> a
  lookup (Cons (One t) ts) i | i < size t = lookupTree t i
                             | otherwise = lookup ts (i - size t)
  lookup (Cons (Two t1 t2) ts) i | i < size t1 = lookupTree t1 i
                                 | i < (size t2)*2 = lookupTree t2 (i-(size t1))
                                 | otherwise = lookup ts (i - (size t1)*2)

  -- update :: RAL a -> Int -> a -> RAL a
  update (Cons (One t) ts) i y | i < size t = Cons (One (updateTree t i y)) ts
                               | otherwise =  Cons (One t) (update ts (i - size t) y)
  update (Cons (Two t1 t2) ts) i y | i < size t1 = 
                                      Cons (Two (updateTree t1 i y) t2) ts
                                   | i < (size t2)*2 =
                                      Cons (Two t1 (updateTree t2  (i - (size t1)) y)) ts
                                   | otherwise = 
                                      Cons (Two t1 t2) (update ts (i - (size t1)*2) y)

  --fromList :: [a] -> RAL a
  fromList = foldr cons Nil

  -- toList :: RAL a -> [a]
  toList Nil = []
  toList (Cons (One t) ts) = flat t ++ toList ts
  toList (Cons (Two t1 t2) ts) = flat t1 ++ flat t2 ++ toList ts
