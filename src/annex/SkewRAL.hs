{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleContexts #-}
module RALSkew where
import Prelude hiding (lookup)
import Data.List hiding (lookup)
import RandomAccessList

data Tree a = Leaf a
            | Node (Tree a) a (Tree a)


data RAL a = Nil
					 | Cons (Int, Tree a) (RAL a)

flat (Leaf a) = [a]
flat (Node t1 x t2) = x : (flat t1 ++ flat t2)

lookupTree :: Int -> Tree a -> Int -> a
lookupTree 1 (Leaf x) 0 = x
lookupTree _ (Node t1 x t2) 0 = x
lookupTree w (Node t1 x t2) i
             | i <= w = lookupTree (w `div` 2) t1 (i-1)
             | otherwise = lookupTree (w `div` 2) t2 (i - 1 - (w `div` 2))

updateTree :: Int -> Tree a -> Int -> a -> Tree a
updateTree 1 (Leaf x) 0 y  = Leaf y
updateTree _ (Node t1 x t2) 0 y = Node t1 y t2
updateTree w (Node t1 x t2) i y
             | i <= w = updateTree (w `div` 2) t1 (i-1) y
             | otherwise = updateTree (w `div` 2) t2 (i - 1 - (w `div` 2)) y



instance RandomAccessList.RALClass RAL a where

	cons x ts@(Cons (w1,t1) (Cons (w2,t2) ts')) 
									| w1 == w2 = Cons (1+w1+w2, (Node t1 x t2)) ts'
									| otherwise = Cons (1, Leaf x) ts
	cons x l = Cons (1, Leaf x) l

	head (Cons (1, Leaf x) _) = x
	head (Cons (w, Node _ x _) _) = x

	tail (Cons (1, Leaf _) ts) = ts
	tail (Cons (w, Node t1 x t2) ts) = Cons (w `div` 2, t1) (Cons (w `div` 2, t2) ts)


	lookup (Cons (w, t) ts) i
					| i < w = lookupTree w t i
					| otherwise = lookup ts (i-w)

	update (Cons (w, t) ts) i y
					| i < w = Cons (w, updateTree w t i y) ts
					| otherwise = Cons (w, t) (update ts (i-w) y)

	fromList = foldr cons Nil

	toList ts = concat (map (\(w,t)-> flat t) (unfoldr tolst ts))
		where
		tolst Nil = Nothing
		tolst (Cons x xs) = Just (x, xs)  
