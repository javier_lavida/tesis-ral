{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleContexts #-}
module RALNestedFO where
import Prelude hiding (lookup)
import RandomAccessList

data RAL a = Nil
           | Zero (RAL (a,a))
           | One a (RAL (a,a))
           deriving Show

len :: RAL a -> Integer
len Nil = 0
len (Zero l) = 2*(len l)
len (One _ xs) = 1 + 2*(len xs)

uncons :: RAL a -> (a, RAL a)
uncons (One x Nil) = (x, Nil)
uncons (One x ps) = (x, Zero ps)
uncons (Zero ps) = (x, One y ps') where ((x,y), ps') = uncons ps

instance RandomAccessList.RALClass RAL a where

  --cons :: a -> RAL a -> RAL a
  cons x Nil = One x Nil
  cons x (Zero ps) = One x ps
  cons x (One  y ps) = Zero (cons (x,y) ps)

  
  head xs = x where (x, _) = uncons xs
  tail xs = xs' where (x,xs') = uncons xs

  --lookup :: RAL a -> Integer -> a
  lookup (One x _)  0 = x
  lookup (One _ xs) i = lookup (Zero xs) (i-1)
  lookup (Zero xs)  i = let (x,y) = lookup xs (i `div` 2)
                          in if (i `mod` 2) == 0 then x else y

  --update :: RAL a -> Integer -> a -> RAL a
  update (One x xs) 0 e = One e xs
  update (One x xs) i e = cons x (update (Zero xs) (i-1) e)
  update (Zero xs)  i e = let
                           (x,y) = lookup xs (i `div` 2)
                           p = if (i `mod` 2) == 0 then (e,y) else (x,e)
                          in
                           Zero (update xs (i-1) p)

  --fromList :: [a] -> RAL a
  fromList = foldr cons Nil

  --toList :: RAL a -> [a]
  toList Nil = []
  toList l = let (x, xs) = uncons l in x:(toList xs)


superUpdate :: Integer -> a -> RAL a -> RAL a
superUpdate i e xs = fupdate (\x -> e) i xs

fupdate :: (a -> a) -> Integer -> RAL a -> RAL a
fupdate f 0 (One x xs) = One (f x) xs
fupdate f i (One x xs) = cons x (fupdate f (i-1) (Zero xs))
fupdate f i (Zero xs) = let
                          f' = \(x,y) -> if (i `mod` 2) == 0 then (f x, y) else (x, f y)
                        in
                          Zero (fupdate f' (i `div` 2) xs) 



