

data RAL a = Nil
           | Zero (RAL (a,a))
           | One a (RAL (a,a))
           deriving Show

len :: RAL a -> Integer
len Nil = 0
len (Zero l) = 2*(len l)
len (One _ xs) = 1 + 2*(len xs)

cons :: a -> RAL a -> RAL a
cons x Nil = One x Nil
cons x (Zero ps) = One x ps
cons x (One  y ps) = Zero (cons (x,y) ps)

uncons :: RAL a -> (a, RAL a)
uncons (One x Nil) = (x, Nil)
uncons (One x ps) = (x, Zero ps)
uncons (Zero ps) = (x, One y ps')
                    where ((x,y), ps') = uncons ps

head xs = x where (x, _) = uncons xs
tail xs = xs' where (_,xs') = uncons xs

fromList :: [a] -> RAL a
fromList = foldr cons Nil

toList :: RAL a -> [a]
toList Nil = []
toList l = let (x, xs) = uncons l in x:(toList xs)

lookup :: Integer -> RAL a -> a
lookup 0 (One x _) = x
lookup i (One _ xs) = lookup (i-1) (Zero xs)
lookup i (Zero xs) = let (x,y) = lookup (i `div` 2) xs
                in if (i `mod` 2) == 0 then x else y

update :: Integer -> a -> RAL a -> RAL a
update 0 e (One x xs) = One e xs
update i e (One x xs) = cons x (update (i-1) e (Zero xs))
update i e (Zero xs) = 
  let
    (x,y) = lookup (i `div` 2) xs
    p = if (i `mod` 2) == 0 then (e,y) else (x,e)
  in
    Zero (update (i-1) p xs)

superUpdate :: Integer -> a -> RAL a -> RAL a
superUpdate i e xs = fupdate (\x -> e) i xs

fupdate :: (a -> a) -> Integer -> RAL a -> RAL a
fupdate f 0 (One x xs) = One (f x) xs
fupdate f i (One x xs) = cons x (fupdate f (i-1) (Zero xs))
fupdate f i (Zero xs) = let
                          f' = \(x,y) -> if (i `mod` 2) == 0 then (f x, y) else (x, f y)
                        in
                          Zero (fupdate f' (i `div` 2) xs) 

snoc :: a -> RAL a -> RAL a
snoc e Nil = One e Nil
snoc e xs = let (y,ys) = uncons xs in cons y (snoc e ys)

rev :: RAL a -> RAL a
rev Nil = Nil
rev xs = let (y,ys) = uncons xs in snoc y (rev ys)