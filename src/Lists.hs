

data List a = Nil
  | Cons a (List a)


cons :: a -> List a -> List a
cons a l = Cons a l

head :: List a -> a
head (Cons a l) = a

tail :: List a -> List a
tail (Cons a l) = l

lookup :: Int -> List a -> a
lookup 0 l = head l
lookup i l = lookup (i-1) (tail l)

update :: Int -> a -> List a -> List a
update 0 x l = cons x (tail l)
update i x l = cons (head l) (update (i-1) x (tail l))  