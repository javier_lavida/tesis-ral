
type Nat = [Int]

carry :: Int -> Nat -> Nat
carry w [] = [w]
carry w ws@(w1 : ws1) = if w < w1 then w : ws
                        else carry (w*2) ws1 

borrow :: Int -> Nat -> Nat
borrow w ws@(w1 : ws1) = if w == w1 then ws1
                        else w : (borrow (2*w) ws1)

inc :: Nat -> Nat
inc ws = carry 1 ws

dec :: Nat -> Nat
dec ws = borrow 1 ws

check :: Nat -> Bool
check [] = True
check l = checkAux l (takeWhile (<= m) powersOfTwo) 
      where m = maximum l

powersOfTwo :: [Int]
powersOfTwo = 1 : (map (\x->2*x) powersOfTwo)

checkAux :: Nat -> [Int] -> Bool
checkAux [] _ = True
checkAux _ [] = False
checkAux (x:xs) pows =
    elem x pows && checkAux xs (dropWhile ( <= x) pows)
         