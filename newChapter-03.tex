\chapter{Sistemas numéricos}

Cuando utilizamos números naturales usualmente los representamos con combinaciones de los dígitos del 0 al 9. Esto corresponde a la representación en el sistema numérico decimal. Este sistema no es el único que existe. En este capítulo utilizaremos diversos sistemas para representar los naturales, los cuales nos llevarán a diversas implementaciones de estructuras de datos, conocidas como representaciones numéricas.

\section{Sistemas numéricos no posicionales}
Cuando éramos infantes nos enseñaron a contar con los dedos o utilizando ``palitos''. Entonces para representar el número $n$ teníamos $n$ palitos, no importaba el orden de los palitos ya que todos los palitos eran iguales. Este concepto está formalizado utilizando la siguiente definición:
\begin{itemize}
\item El cero es un número natural.
\item El sucesor de un número natural es un natural.
\end{itemize}
Con esta definición, la analogía es: El cero representa no tener palitos y aumentar un palito es equivalente a obtener el sucesor de la cantidad de palitos que teníamos antes.
La definición formal en \textsc{Haskell} es:
\begin{lstlisting}
data Nat =  Zero
          | Succ Nat
\end{lstlisting}

Ahora bien, para nuestros propósitos requerimos las operaciones de suma y sucesor para estos números. La operación sucesor es inmediata a partir de la definición del tipo ya que queda implementada mediante el constructor \texttt{Succ}. Para la función suma nos servimos de la siguiente definición recursiva. Sumarle cero a un número resulta en el mismo número, y sumarle el sucesor de $m$ a $n$ es equivalente a obtener el sucesor de la suma de $n$ y $m$. Es decir: $ n + (\texttt{Succ}\ m) = \texttt{Succ} (n+m)$. Definiéndolo formalmente en \textsc{Haskell} obtenemos:

\begin{lstlisting}

add :: Nat -> Nat -> Nat
add n Zero = n
add n (Succ m) = Succ (add n m)
\end{lstlisting}

Ahora bien, puesto que la construcción del sucesor de un número se realiza de manera sencilla añadiendo un constructor \texttt{Succ}, la definición del predecesor se hace de manera directa eliminándolo:

\begin{lstlisting}

pred :: Nat -> Nat
pred (Succ n) = n
pred Zero = error "Predecesor of zero"
\end{lstlisting}

Nótese que el predecesor no está definido en cero. Podríamos manejar el error definiendo como \texttt{Zero} al predecesor de él mismo. Sin embargo, para propósitos de este documento, dejaremos este caso como un error.

A simple vista un sistema numérico no posicional es muy rudimentario, pero más adelante veremos como una estructura de datos muy común corresponde a una representación de este sistema. 
 
\section{Sistemas numéricos posicionales}

Retomando el sistema numérico decimal tradicional es claro ver que, por ejemplo, el número 123 no es igual al número 231 ni al número 321 aunque los tres utilicen los mismos dígitos y en la misma cantidad. Esto se debe a que el sistema numérico decimal es un sistema numérico posicional, es decir, el dígito tiene distinto peso según su posición en la sucesión de dígitos. 

En general, un sistema numérico posicional es un par $\mathcal{S}=\pt{D,W}$ donde:
\begin{itemize}
\item
$D \neq \varnothing$ es un conjunto cuyos elementos llamaremos dígitos. Usualmente $D$ es un subconjunto de los naturales del 0 al 9.
\item
$W \neq \varnothing$, $W\subset \mathbb{R}$ es un conjunto de pesos, usualmente infinito.
\end{itemize}

Una vez fijado un sistema numérico $\mathcal{S}=\pt{D,W}$, una sucesión de dígitos $d_0d_1d_2\dots d_n$, donde $d_i \in D$, representa al número dado por:
\[
\sum\limits_{i=0}^n d_i \cdot w_i
\]

Introducimos la notación $n\approx d_0d_1d_2\dots d_k$ que significará que el entero $n$ está representado por $d_0d_1d_2\dots d_k$, es decir, $n=\sum\limits_{i=0}^k d_i \cdot w_i$

Es importante recalcar que, a lo largo de este documento, consideraremos que si $n\approx d_0d_1d_2\dots d_k$, entonces $d_0$ es el dígito menos significativo y $d_k$ es el dígito más significativo de $n$, es decir, $d_0$ tendrá asociado el peso más bajo y $d_k$ el más alto. Esta convención es a la inversa de los sistemas numéricos tradicionales. La cual es común en el estudio de los sistemas numéricos en abstracto y seguimos aquí por simplicidad.

Veamos algunos ejemplos de sistemas numéricos: 

\begin{itemize}

\item Sistema numérico decimal común: $D=\{0,\dots, 9\},\;W=\{ 10^i\, |\, i\in\mathbb{N}\}$.

\item Sistema binario común: $D=\{0,1\},\;W=\{2^i\;|\;i\in\mathbb{N}\}$. De modo que $5\approx 101$. %= 1\cdot 2^0 + 0\cdot 2^1 + 2\cdot 2^2
\item Sistema binario con $1,2$ (sin cero): $D=\{1,2\},\;W=\{2^i\;|\;i\in\mathbb{N}\}$. De modo que $5\approx 12$. %= 1\cdot 2^0 + 2\cdot 2^1 
\item Sistema binario con $0,1,2$ : $D=\{0,1,2\},\;W=\{2^i\;|\;i\in\mathbb{N}\}$. De modo que $5\approx 12$, $5 \approx 101$.
\item Sistema binario de Fibonacci: $D=\{0,1\},\;W=\{F_i\;|\;i\in\mathbb{N}\}$ donde
$F_i$ es el i-ésimo número de Fibonacci ($1,1,2,3,5,8,13,\ldots$). De modo que $5 \approx 00001$, $5 \approx 0011$, $7\approx 00101$.
\end{itemize}

Notemos que en algunos sistemas la representación de un número no es única. Cuando esto suceda, diremos que el sistema es \emph{redundante}

% REVISADO 03-Feb-15

\section{Representación densa y dispersa}

Con los sistemas posicionales, hay dos maneras distintas de representar un número natural: la representación {\it densa} y la representación {\it dispersa}. En la primera haremos explícitos los dígitos nulos. En cambio, en la segunda omitiremos estos dígitos. Para ello, la representación deberá tener algún indicador de los pesos de cada dígito distinto de cero.

Para el sistema binario común, la representación usual es una representación \emph{densa}, por ejemplo $14 \approx 0111$. Por otro lado, una representación dispersa se obtiene al representar un número mediante la lista de los pesos de los dígitos no nulos. Por ejemplo el $14 \approx [2,\ 4,\ 8]$.

Para nuestro propósito final nos serán de especial interés ambas formas de representación para sistemas binarios. Empezaremos analizando más a fondo los primeros.

Empecemos definiendo una representación densa en \textsc{Haskell}:

\lstinputlisting[firstline=2, lastline=5]{src/Dense.hs}
De esta manera, un número es representado por una lista de \texttt{Zero} y \texttt{One}. Por ejemplo el 14, quedaría implementado mediante la lista $\texttt{[Zero, One, One, One]}$. En seguida discutiremos la implementación de las operaciones básicas (incremento y decremento). Para obtener el incremento en uno, o sucesor de un número basta fijarnos en los casos posibles: Si tenemos una sucesión vacía de dígitos, la cual representa al 0, deberemos regresar una sucesión incluyendo únicamente al dígito \texttt{One}; si tenemos una sucesión que inicia con \texttt{Zero}, solo sustituimos éste por el dígito \texttt{One}. El caso interesante se da cuando la lista inicia con \texttt{One}. Recordemos cuando aprendimos a sumar, dejando de un lado que el orden de sumar era de derecha a izquierda, podremos recordar que cuando sumábamos 1 a un 9, colocábamos un 0 y {\it llevábamos} 1, este 1 debía sumarse al resultado de la suma de los siguientes dígitos. Consideremos la siguiente figura:
\begin{center}
\includegraphics[scale=0.6]{img/Suma}
\end{center}
Empezamos sumando 9 y 1. Colocamos un 0 pero llevamos 1. Luego procedemos a sumar el par de unos así que deberíamos colocar un 2 pero llevábamos 1, por lo tanto colocamos el 3. Esta operacion de {\it llevar 1} es conocida como operación de \emph{acarreo}. 

Esta es la misma operación que debemos ejecutar cuando queremos añadir 1 a una sucesión de dígitos que inicia con \texttt{One}. Solo que lo estamos realizando en orden inverso (de izquierda a derecha) dado que el dígito menos significativo está a la izquierda. Por lo tanto, cuando tenemos una sucesión que inicia con \texttt{One} colocamos un \texttt{Zero} y añadimos \texttt{One} al resto de la sucesión. La implementación queda así:

\lstinputlisting[firstline=7, lastline=11]{src/Dense.hs}

Para decrementar la idea es similar, los casos de sucesiones que inician con \texttt{One} son sencillos. Basta sustituir el \texttt{One} con \texttt{Zero} o regresar la lista vacía cuando la sucesión sólo tenía un \texttt{One}. Cuando la sucesión empieza con \texttt{Zero} hay que realizar una operación similar al acarreo. Recordemos nuestra infancia con el siguiente ejemplo:

\begin{center}
\includegraphics[scale=0.6]{img/Resta}
\end{center}

Al restarle 1 al 0, no tenemos alternativa más que poner el dígito 9 y decir: {\it Le pedimos prestado} al siguiente dígito, que en este caso particular es 2. Así cuando restamos 1 al 2, el resultado no es 1, porque le habíamos pedido prestado 1 al 2, es decir, el 2 se convierte en 1 por lo que en realidad restamos 1 a 1 y el resultado es 0. Así para nuestra implementación, cuando restemos 1 a una sucesión que inicia con \texttt{Zero}, sustituimos el \texttt{Zero} por \texttt{One} y le pedimos prestado al resto de la sucesión, i.e., la decrementamos en 1. La implementación es:

\lstinputlisting[firstline=12, lastline=17]{src/Dense.hs}

Pasemos ahora al caso de la implementación dispersa. Representamos un número binario mediante su lista ascendente de pesos. Definiremos las mismas operaciones para estos sistemas numéricos.

Añadir 1 a una sucesión de pesos es fácil si ésta no contiene el peso 1, pero si lo contiene ¿qué hacemos? La solución consiste en darnos cuenta que tenemos dos pesos 1, los cuales pueden sustituirse por un solo peso 2. Entonces, en lugar de añadir el peso 1 a la lista que ya contiene un peso 1, quitamos dicho peso de la sucesión y agregamos el peso 2. Si la sucesión ya contenía un peso 2, procedemos igual: quitamos el peso 2 y añadimos un peso 4, y así sucesivamente hasta que no sea necesario juntar los pesos.

Decrementar una sucesión es similar. Si la sucesión contiene el peso 1, lo quitamos y habremos terminado. Si no es así, debemos obtener un peso 1 del resto de la sucesión. ¿Cómo obtenemos un peso 1? La manera de obtener un 1, es obtener un peso 2 de la sucesión y descomponerlo en dos pesos 1. Dejamos un peso 1 en la sucesión y el otro lo quitamos; así habremos restado 1 a la sucesión. ¿Qué ocure cuando la sucesión no tiene un peso 2 para ser descompuesto en 2 pesos 1? Entonces deberemos obtener ese peso 2 de la sucesión descomponiendo un peso 4 en dos pesos 2; si no hay un peso 4, buscamos un peso 8 para descomponerlo en dos pesos 4; y así sucesivamente.

La implementación dispersa completa en \textsc{Haskell} es:

\lstinputlisting[firstline=1, lastline=22]{src/Sparse.hs}

Es inmediato ver que nuestra implementación tiene un problema, puesto que un número se representa mediante una lista arbitraria de enteros, los cuales no son necesariamente potencias de 2. Para intentar solucionar este problema, definimos la función \texttt{check} que decidirá si una lista de enteros es un natural válido o no. Para ello, tenemos dos posibles casos:
\begin{itemize}
\item Una lista vacía es un natural válido dado que ésta representa al 0.
\item Una lista no vacía es válida si sus elementos son potencias de 2 en orden ascendente sin repeticiones.
\end{itemize}
Para implementar esto, hacemos uso de dos funciones auxiliares: \texttt{powersOfTwo} y \texttt{checkAux}. La primera únicamente devuelve una lista infinita de las potencias de 2, lo cual es posible gracias a la evaluación perezosa. La segunda, recibirá la lista de dígitos sin verificar y una lista de potencias de 2 finita. Al inicio, los dígitos sin verificar serán todos los dígitos, y la lista de potencias de 2 todas las potencias de 2 menores o iguales que $m$, donde $m$ es el elemento más grande de la lista por verificar. Ahora bien, para una lista no vacía, se verificará que la cabeza sea un elemento de la lista de potencias de 2 finita. Si no es potencia de 2, es claro que el natural no es válido. Si lo es, volvemos a aplicar \texttt{checkAux} con los dígitos restantes y la misma lista de potencias de 2, quitando los elementos menores o iguales que el primer dígito.

Así, la definición en \textsc{Haskell} será:

\lstinputlisting[firstline=23]{src/Sparse.hs}

Más adelante mostraremos una implementación que utiliza tipos de datos más sofisticados, lo cual evitará la necesidad de verficar si una lista es válida o no. 

\section{Números binarios sesgados}

A continuación presentaremos un sistema numérico binario que nos resultará muy útil para nuestro objetivo final, dado que las operaciones de incremento y decremento son más eficientes. Aunque los conjuntos de pesos y dígitos se vuelven más complejos.

Cambiaremos el conjunto de pesos $\{2^i \,|\, i \in \mathbb{N} \}$ por el conjunto $W=\{ 2^{i+1}-1 \,|\, i \in \mathbb{N} \}$. También, utilizaremos otros dígitos, a saber $D=\{0, 1, 2\}$. A primera instancia podemos ver que este sistema es redundante. Por ejemplo el número decimal $8$ puede ser representado como $220$ ó $101$.

Para quitar esta ambigüedad añadiremos una restricción más: \emph{El dígito 2 sólo puede aparecer una vez y debe ser el primer dígito distinto de cero}. Cuando un número binario sesgado cumple esta última restricción diremos que está en su forma \emph{canónica}. Empecemos mostrando que la representación canónica existe y es única.

\begin{lemma}
Sea $\alpha$ un número binario sesgado en su representación canónica. Si $\alpha$ tiene $n$ dígitos y $\alpha \approx A$ entonces $2^n-1\leq A \leq 2^{n+1}-2$.
\end{lemma}
\begin{proof}

Primero veamos que:\\
$2^n-1\leq A$\\
Es claro que el número más pequeño que se puede representar con $n$ dígitos es:
\[
\beta = 0 \dots 01
\]

Por lo que, como $\beta \approx 2^{n-1}$, se sigue que $2^{n-1} \leq A$.

Ahora veamos que se cumple que $A \leq 2^{n+1}-2$.\\

De manera opuesta a la desigualdad anterior, buscaremos una sucesión de $n$ dígitos $\beta$ con valor máximo. En tal caso $\beta=0^k21^j$, con $j+k+1=n$.

Pero observemos también que:\\
$
0^k21^j \approx \sum\limits_{i=k+2}^n (2^i-1) + 2(2^{k+1}-1) \\
= \sum\limits_{i=k+2}^n 2^i - \sum\limits_{i=k+2}^n 1 + 2(2^{k+1}-1)\\
= \sum\limits_{i=0}^n 2^i - \sum\limits_{i=0}^{k+1} 2^i - \sum\limits_{i=k+2}^n 1 + 2(2^{k+1}-1)
$\\
Además recordemos que:
$
\sum\limits_{i=0}^n 2^i = 2^{n+1}-1 
$\\
Así:
$
0^k21^j \approx (2^{n+1}-1) - (2^{k+2}-1) - (n-(k+2)) + 2(2^{k+1}-1) \\
= (2^{n+1}-1) - 2^{k+2} + 1  - n + k + 2 + 2^{k+2} - 2 \\
= 2^{n+1} - 1 - (n-k) \\
\leq 2^{n+1} - 2
$ Dado que $ k \leq n-1$

De esto podemos concluir que $\beta=0^n2$ y además $\beta \approx 2^{n+1}-2$
\end{proof}

\begin{lemma}

Si $A\leq 2^{n+1}-2$ entonces existe $\alpha$ tal que $\alpha \approx A$.

\end{lemma}

\begin{proof}
Inducción sobre $n$.
Base $n=1$.
$A \leq 2^2-2 = 2$ entonces $A\in\{0,1,2\}$.
Si $A=0$, entonces $\alpha$ es la secuencia vacía y como $1\approx 1$ y $2 \approx 2$, la afirmación es válida.

Hipótesis de inducción.
Si $A\leq2^{n+1}-2$, entonces existe $\alpha$ tal que $\alpha \approx A$.

Supongamos ahora que $A\leq 2^{n+2}-2$. Entonces, tenemos 2 casos:
\begin{itemize}
\item $A=2^{n+2}-2$. 
Sea $\alpha=0^n2$. Así $\alpha\approx 2(2^{n+1}-1) = 2^{n+2}-2$.
\item $A\leq 2^{n+2}-3$.

Por la hipótesis de inducción, si $A \leq 2^{n+1}-2$, $\alpha$ existe. Por lo tanto, basta analizar el caso cuando
$2^{n+1}-1\leq A \leq 2^{n+2}-3$.

Sea $B=A-2^{n+1}+1$. Es fácil ver que $B\leq 2^{n+1}-2$. De donde, por la HI, existe $\beta$ tal que $\beta\approx B$.
Además, por el lema anterior, como $B \leq 2^{n+1}-2$ entonces $\beta$ tiene $k$ dígitos con $k\leq n$.

Así proponemos la sucesión:
$\alpha = \beta 0^{n-k}1$. Veamos pues que $\alpha \approx A$.

$\alpha = \beta 0^{n-k}1 \approx B + 2^{n+1}-1 = A - 2^{n+1} + 1 + 2^{n+1} - 1 = A$.
$\therefore \alpha = \beta 0^{n-k}1 \approx A$
\end{itemize}
\end{proof}

\begin{proposition}
Sean $\alpha$ y $\beta$ representaciones canónicas.
Si $\alpha \approx A$ y $\beta \approx B$ y $\alpha \neq \beta$, entonces $A\neq B$. 
\end{proposition}

\begin{proof}

Para efectos de esta demostración, utilizaremos 2 notaciones no mencionadas anteriormente. La notación $|\alpha|$ significará la cantidad de dígitos de $\alpha$ y la notación $[\alpha]$ será el número representado por $\alpha$, es decir, si $\alpha \approx A$, entonces $[\alpha] = A$.

Caso 1. $|\alpha| \neq |\beta|$

SPG, supongamoes que $|\beta| + 1 \leq |\alpha|$
Por el lema anterior, tenemos que:\\
$[\beta]\leq 2^{\beta + 1}-2 < 2^{\beta + 1} -1 \leq 2^{|\alpha|}-1 \leq [\alpha]$\\
$\Rightarrow [\beta] < [\alpha]$\\
$\therefore [\alpha] \neq [\beta]$.\\

Caso 2. $|\alpha| = |\beta|$.
$\exists \rho, \tau, \pi$ tales que:\\
$\alpha = \tau a \rho$\\
$\beta = \pi b \rho$\\
$|\tau| = |\pi| = n$\\
$b+1\leq a$. Aquí, $b+1$ se refiere al dígito sucesor de $b$.

$[\pi b] = [\pi] + [0^nb]$\\
Además, por el lema anterior sabemos que $[\pi] \leq 2^{n+1}-2 < 2^{n+1}-1$.\\
Así, $[\pi] + [0^nb] < (2^{n+1} - 1) + [0^nb] = [0^n(b+1)] \leq [0^na]$.\\

Con esto, podemos ver que

$[\beta] = [\pi b \rho] = [\pi b] + [0^{n+1}\rho]$\\
y por el argumento anterior\\
$ < [0^na]+[0^{n+1}\rho] \leq [\tau a \rho] = [\alpha]$\\
$\Rightarrow [\beta] < [\alpha]$\\
$\therefore [\alpha] \neq \beta$


\end{proof}

Veamos ahora unos ejemplos de números binarios sesgados en su representación canónica:
\begin{itemize}
\item $ 10 \approx 011$
\item $ 11 \approx 111$
\item $ 12 \approx 211$
\item $ 13 \approx 021$
\item $ 14 \approx 002$
\end{itemize}

Discutimos en seguida la implementación de las funciones de incremento y decremento, trabajando únicamente con representaciones canónicas. Cada dígito tiene peso $2^{i+1}-1$ y notemos que $1+2(2^{i+1}-1) = 2^{i+2}-1$. A partir de esto, podemos ver que incrementar un número binario sesgado cuyo primer dígito distinto de cero es 2 se puede hacer fácilmente sustituyendo el 2 por 0 e incrementar el siguiente dígito de 0 a 1 o de 1 a 2 según corresponda. Incrementar un número binario sesgado que no contenga un 2 es más sencillo: simplemente hay que incrementar el primer dígito de 0 a 1 o de 1 a 2. En ambos casos, el número resultante está en su forma canónica y asumiendo que podemos encontrar el primer dígito distinto de cero en $O(1)$ ambos casos se ejecutan en $O(1)$.

Si usáramos una implementación densa, el primer dígito distinto de cero no podría encontrarse en $O(1)$, por lo que elegimos una implementación dispersa. Nuevamente representaremos un número binario sesgado mediante una lista de enteros, cuyos elementos serán los pesos. Veamos los ejemplos anteriores con esta implementación.

\begin{itemize}
\item $ [3,7] \approx 10$
\item $ [1,3,7] \approx 11$
\item $ [1,1,3,7] \approx 12$
\item $ [3,3,7] \approx 13$
\item $ [7,7] \approx 14$
\end{itemize}

En general, definimos a los naturales como una lista de enteros, pensando que los enteros en la lista deben ser de la forma requerida, es decir, de la forma $2^{i+1}-1$.

\begin{lstlisting}
data Nat = [Int]
\end{lstlisting}


Así definimos la función sucesor como sigue:

\label{succ_skew}
\begin{lstlisting}
succ :: Nat -> Nat
succ w1:w2:ws | w1 == w2 = (1+w1+w2):ws
              | otherwise = 1:w1:w2:ws
succ ws = 1:ws 
\end{lstlisting}
El primer caso verifica si los primeros dos pesos son iguales; si es así los combina para formar un peso del siguiente rango, en otro caso solo añade 1 al principio. El segundo resuelve el caso de la lista vacía o un solo elemento. Claramente ambos casos se ejecutan en $O(1)$.

Para decrementar tenemos dos casos: Si el primer peso de la lista es 1, el decremento es trivial quitándolo. En otro caso, tendremos un elemento de la forma $2^{i+1}-1$. Necesitamos obtener $2^{i+1}-2=2(2^i-1)$. Y ya que, $\floor{\frac{2^{i+1}-1}{2}} = 2^i-1$, agregaremos 2 veces el resultado de dividir entre 2 el primer peso de la lista.

\label{pred_skew}
\begin{lstlisting}
pred :: Nat -> Nat
pred (1:ws) = ws
pred (w: ws) = (w `div` 2) : (w `div` 2) : ws
\end{lstlisting} 

Ahora discutamos como es que conocer los sistemas numéricos puede ayudarnos para construir una estructura que nos permita añadir y quitar elementos eficientemente.


\section{Representaciones numéricas}

Consideremos la representación de números naturales con un sistema numérico no posicional: 

\begin{lstlisting}
data Nat = Zero
  | Succ Nat
\end{lstlisting}

Lo único que recibe el constructor \texttt{Succ} es otro elemento de tipo \texttt{Nat} y el resultado de aplicar dicho constructor es obtener un número natural más grande en una unidad. ¿Por qué hemos nombrado \texttt{Succ}? Sólo por convención y dejar claro que este constructor nos da el sucesor de un natural. ¿Qué ocurriría si le cambiásemos el nombre? Por ejemplo de \texttt{Succ} a \texttt{Cons}, el \texttt{Zero} lo renombraramos como \texttt{Nil} y al tipo mismo lo renombraramos como \texttt{List}. Tendríamos el siguiente resultado:

\begin{lstlisting}
data List = Nil
          | Cons (List)
\end{lstlisting}

El resultado se ve sospechosamente similar a la definición de listas. Lo único que hace falta es que el constructor \texttt{Cons} reciba como parámetro adicional un elemento de \texttt{a}. De esta manera obtenemos la definición usual de listas:

\begin{lstlisting}
data List a = Nil
    | Cons a (List a)
\end{lstlisting}

Hemos obtenido la definición de listas a partir de la definición de los números naturales. Analicemos que ocurre con las operaciones. 

Incrementar un número natural se hace con el constructor \texttt{Succ} mientras que añadir un elemento se hace con el constructor \texttt{Cons}. Descartando el hecho de que \texttt{Cons} recibe un elemento de tipo \texttt{a}, efectivamente incrementar (añadir un elemento) a ambas estructuras se hace de manera análoga.

Por otro lado, para decrementar un número natural, basta quitar el primer constructor \texttt{Succ}. De manera análoga, quitar el primer elemento de una lista, se hace quitando el primer constructor \texttt{Cons} que se encuentra en la lista. 


% Analicemos ahora la suma en números naturales:

% \begin{lstlisting}
% add :: Nat -> Nat -> Nat
% add Zero n = n
% add (Succ n) m = Succ (add n m)
% \end{lstlisting}

% ¿Qué significa {\it sumar} dos listas? Sumar dos listas es equivalente a decir que {\it pegamos} todos los elementos de una lista después de la otra, obteniendo así una sola lista de longitud $n+m$ donde $n$ y $m$ eran las longitudes de las listas originales. Es decir, {\it sumar} dos listas es concatenarlas. 

% Si cambiamos el nombre de la función \texttt{add} a \texttt{append} y cambiamos sus argumentos para que sean de tipo \texttt{List a} obtendríamos:

% \begin{lstlisting}
% append :: List a -> List a -> List a
% append Nil l = l
% append (Cons a l) xs = Cons a (append l xs)
% \end{lstlisting}

% ¡Es la misma operación! Sólo cambiamos nombres y el constructor \texttt{Succ} fue sustituido por \texttt{Cons a}. Esta sospechosa analogía se puede generalizar como veremos a continuación.

Las estructuras que tengan una analogía con alguna representación de los números naturales las llamaremos {\it representaciones numéricas}. La representación de los números naturales no es necesariamente la que vimos en el ejemplo anterior. Nuestro objetivo, después de darnos cuenta de esta analogía, será crear estructuras de datos que hereden las propiedades del sistema numérico en el que estén basadas. Es decir, se cumplirán los siguientes hechos:

\begin{itemize}
\item
Una estructura que contenga $n$ elementos, se modelará a través de la representación del número $n$ en el sistema numérico.
\item
Agregar un elemento a una estructura de tamaño $n$ será análogo a sumar 1 a la representación de $n$.
\item
Eliminar un elemento de una estructura de tamaño $n$ corresponderá a restar 1 a la representación de $n$.
\end{itemize}

A lo largo de este documento desarrollamos distintas implementaciones de la estructura funcional conocida como \emph{lista de acceso aleatorio}, la cual es una lista de árboles que sigue la idea desarrollada al final del capítulo anterior y cuya definición se basa en una representación numérica, es decir, las funciones de agregar y eliminar un elemento, se inspiran en las operaciones de sucesor y predecesor en el sistema numérico. El concepto de representación numérica será utilizado para implementar otras estructuras puramente funcionales, por ejemplo: Montículos binomiales. Para profundizar acerca representaciones numéricas, se puede consultar \cite{okasaki}.

En este capítulo estudiamos los principales conceptos que requeriremos para definir una lista de acceso aleatorio. Aclaramos con precisión el concepto de lista, así como las estructuras llamadas árboles. Estos conceptos deberán estar muy presentes en el siguiente capítulo dado que las listas de acceso aleatorio se definirán usando ambos. Presentamos también el concepto de representaciones numéricas con el que modelaremos las listas de acceso aleatorio. También nos dimos cuenta de que al usar una implementación dispersa de un sistema numérico, el tipado no garantiza que la estructura sea válida, por ejemplo, el tipo de naturales se define simplemente como una lista de enteros; no hay garantía de que sus elementos sean pesos válidos, por ejemplo, potencias de 2. Este problema se hereda a las listas de acceso aleatorio y tendremos que buscarle una solución.
